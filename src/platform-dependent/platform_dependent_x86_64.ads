with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions; use Interfaces.C.Extensions;

package Platform_Dependent
is
   package Integers is
      type mode_t is new Unsigned_32;

      -- Seconds since the Epoch.
      type time_t is new Signed_64;

      -- Microseconds
      type suseconds_t is new Signed_64;

      -- TODO: seems to be unsused
      -- Count of microseconds. Unsigned
      --  type useconds_t is new Unsigned_64; 

      -- Stuff for enum decls
      --
      -- they are usually sizeof (int) in gcc, except for ARM
      -- the C standard allows varying enum sizes like Ada,
      -- but the choice is implementation defined
      type Log_Priority_Underlying is new unsigned;
      type Grab_Mode_Underlying is new unsigned;
      type Led_Value_Underlying is new unsigned;
      type Read_Flags_Underlying is new unsigned;

   end Integers;
end Platform_Dependent;
