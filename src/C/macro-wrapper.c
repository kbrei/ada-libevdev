/* #define _POSIX_C_SOURCE 200809L */
#define _GNU_SOURCE // for vaprintf

#include <fcntl.h>
#include <errno.h>
#include <stddef.h>
#include <linux/input.h>
#include <libevdev-1.0/libevdev/libevdev.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// we need to wrap and link against these macros as constants,
// because their values are/may be archtecture dependent
// and there is no (sane) way to evaulate C macros in ada

// open flags
const int o_rdonly = O_RDONLY;
const int o_wronly = O_WRONLY;
const int o_rdwr = O_RDWR;
const int o_append = O_APPEND;
const int o_async = O_ASYNC;
const int o_cloexec = O_CLOEXEC;
const int o_creat = O_CREAT;
const int o_directory = O_DIRECTORY;
const int o_excl = O_EXCL;
const int o_noctty = O_NOCTTY;
const int o_nofollow = O_NOFOLLOW;
const int o_sync = O_SYNC;
const int o_dsync = O_DSYNC;
const int o_trunc = O_TRUNC;
const int o_nonblock = O_NONBLOCK;

// non-POSIX
/* const int o_largefile = O_LARGEFILE; */
/* const int o_noatime = O_NOATIME; */
/* const int o_direct = O_DIRECT; */
/* const int o_path = O_PATH; */
/* const int o_tmpfile = O_TMPFILE; */


// mode_t values
const mode_t s_irwxu = S_IRWXU;
const mode_t s_irusr = S_IRUSR;
const mode_t s_iwusr = S_IWUSR;
const mode_t s_ixusr = S_IXUSR;
const mode_t s_irwxg = S_IRWXG;
const mode_t s_irgrp = S_IRGRP;
const mode_t s_iwgrp = S_IWGRP;
const mode_t s_ixgrp = S_IXGRP;
const mode_t s_irwxo = S_IRWXO;
const mode_t s_iroth = S_IROTH;
const mode_t s_iwoth = S_IWOTH;
const mode_t s_ixoth = S_IXOTH;
const mode_t s_isuid = S_ISUID;
const mode_t s_isgid = S_ISGID;

// Non-POSIX
/* const mode_t s_isvtx = S_ISVTX; */

// all errno values
const int e2big = E2BIG;
const int eacces = EACCES;
const int eaddrinuse = EADDRINUSE;
const int eaddrnotavail = EADDRNOTAVAIL;
const int eafnosupport = EAFNOSUPPORT;
const int eagain = EAGAIN;
const int ealready = EALREADY;
const int ebade = EBADE;
const int ebadf = EBADF;
const int ebadfd = EBADFD;
const int ebadmsg = EBADMSG;
const int ebadr = EBADR;
const int ebadrqc = EBADRQC;
const int ebadslt = EBADSLT;
const int ebusy = EBUSY;
const int ecanceled = ECANCELED;
const int echild = ECHILD;
const int echrng = ECHRNG;
const int ecomm = ECOMM;
const int econnaborted = ECONNABORTED;
const int econnrefused = ECONNREFUSED;
const int econnreset = ECONNRESET;
const int edeadlk = EDEADLK;
const int edeadlock = EDEADLOCK;
const int edestaddrreq = EDESTADDRREQ;
const int edom = EDOM;
const int edquot = EDQUOT;
const int eexist = EEXIST;
const int efault = EFAULT;
const int efbig = EFBIG;
const int ehostdown = EHOSTDOWN;
const int ehostunreach = EHOSTUNREACH;
const int eidrm = EIDRM;
const int eilseq = EILSEQ;
const int einprogress = EINPROGRESS;
const int eintr = EINTR;
const int einval = EINVAL;
const int eio = EIO;
const int eisconn = EISCONN;
const int eisdir = EISDIR;
const int eisnam = EISNAM;
const int ekeyexpired = EKEYEXPIRED;
const int ekeyrejected = EKEYREJECTED;
const int ekeyrevoked = EKEYREVOKED;
const int el2hlt = EL2HLT;
const int el2nsync = EL2NSYNC;
const int el3hlt = EL3HLT;
const int el3rst = EL3RST;
const int elibacc = ELIBACC;
const int elibbad = ELIBBAD;
const int elibmax = ELIBMAX;
const int elibscn = ELIBSCN;
const int elibexec = ELIBEXEC;
const int eloop = ELOOP;
const int emediumtype = EMEDIUMTYPE;
const int emfile = EMFILE;
const int emlink = EMLINK;
const int emsgsize = EMSGSIZE;
const int emultihop = EMULTIHOP;
const int enametoolong = ENAMETOOLONG;
const int enetdown = ENETDOWN;
const int enetreset = ENETRESET;
const int enetunreach = ENETUNREACH;
const int enfile = ENFILE;
const int enobufs = ENOBUFS;
const int enodata = ENODATA;
const int enodev = ENODEV;
const int enoent = ENOENT;
const int enoexec = ENOEXEC;
const int enokey = ENOKEY;
const int enolck = ENOLCK;
const int enolink = ENOLINK;
const int enomedium = ENOMEDIUM;
const int enomem = ENOMEM;
const int enomsg = ENOMSG;
const int enonet = ENONET;
const int enopkg = ENOPKG;
const int enoprotoopt = ENOPROTOOPT;
const int enospc = ENOSPC;
const int enosr = ENOSR;
const int enostr = ENOSTR;
const int enosys = ENOSYS;
const int enotblk = ENOTBLK;
const int enotconn = ENOTCONN;
const int enotdir = ENOTDIR;
const int enotempty = ENOTEMPTY;
const int enotsock = ENOTSOCK;
const int enotsup = ENOTSUP;
const int enotty = ENOTTY;
const int enotuniq = ENOTUNIQ;
const int enxio = ENXIO;
const int eopnotsupp = EOPNOTSUPP;
const int eoverflow = EOVERFLOW;
const int eperm = EPERM;
const int epfnosupport = EPFNOSUPPORT;
const int epipe = EPIPE;
const int eproto = EPROTO;
const int eprotonosupport = EPROTONOSUPPORT;
const int eprototype = EPROTOTYPE;
const int erange = ERANGE;
const int eremchg = EREMCHG;
const int eremote = EREMOTE;
const int eremoteio = EREMOTEIO;
const int erestart = ERESTART;
const int erofs = EROFS;
const int eshutdown = ESHUTDOWN;
const int espipe = ESPIPE;
const int esocktnosupport = ESOCKTNOSUPPORT;
const int esrch = ESRCH;
const int estale = ESTALE;
const int estrpipe = ESTRPIPE;
const int etime = ETIME;
const int etimedout = ETIMEDOUT;
const int etxtbsy = ETXTBSY;
const int euclean = EUCLEAN;
const int eunatch = EUNATCH;
const int eusers = EUSERS;
const int ewouldblock = EWOULDBLOCK;
const int exdev = EXDEV;
const int exfull = EXFULL;

// wrapper functions because errnos are macros in glibc...
int get_errno (){
	return errno;
}

void clear_errno (){
	errno = 0;
}

void set_errno (int new_errno){
	errno = new_errno;
}

const char* format_to_str (const char* format, va_list args)
{
	char* message;
	vasprintf(&message, format, args);

	return message;
}

struct led_action {
	int led_code;
	enum libevdev_led_value led_state;
};

int libevdev_kernel_set_led_values_array(
	struct libevdev *dev,
	struct led_action *actions,
	unsigned int num_actions)
{
	switch (num_actions)
	{
	case 1: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		-1);
	case 2: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		-1);
	case 3: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		actions[2].led_code, actions[2].led_state,
		-1);
	case 4: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		actions[2].led_code, actions[2].led_state,
		actions[3].led_code, actions[3].led_state,
		-1);
	case 5: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		actions[2].led_code, actions[2].led_state,
		actions[3].led_code, actions[3].led_state,
		actions[4].led_code, actions[4].led_state,
		-1);
	case 6: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		actions[2].led_code, actions[2].led_state,
		actions[3].led_code, actions[3].led_state,
		actions[4].led_code, actions[4].led_state,
		actions[5].led_code, actions[5].led_state,
		-1);
	case 7: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		actions[2].led_code, actions[2].led_state,
		actions[3].led_code, actions[3].led_state,
		actions[4].led_code, actions[4].led_state,
		actions[5].led_code, actions[5].led_state,
		actions[6].led_code, actions[6].led_state,
		-1);
	case 8: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		actions[2].led_code, actions[2].led_state,
		actions[3].led_code, actions[3].led_state,
		actions[4].led_code, actions[4].led_state,
		actions[5].led_code, actions[5].led_state,
		actions[6].led_code, actions[6].led_state,
		actions[7].led_code, actions[7].led_state,
		-1);
	case 9: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		actions[2].led_code, actions[2].led_state,
		actions[3].led_code, actions[3].led_state,
		actions[4].led_code, actions[4].led_state,
		actions[5].led_code, actions[5].led_state,
		actions[6].led_code, actions[6].led_state,
		actions[7].led_code, actions[7].led_state,
		actions[8].led_code, actions[8].led_state,
		-1);
	case 10: return libevdev_kernel_set_led_values(
		dev,
		actions[0].led_code, actions[0].led_state,
		actions[1].led_code, actions[1].led_state,
		actions[2].led_code, actions[2].led_state,
		actions[3].led_code, actions[3].led_state,
		actions[4].led_code, actions[4].led_state,
		actions[5].led_code, actions[5].led_state,
		actions[6].led_code, actions[6].led_state,
		actions[7].led_code, actions[7].led_state,
		actions[8].led_code, actions[8].led_state,
		actions[9].led_code, actions[9].led_state,
		-1);
	default:
		abort();
	}

}

const int clock_realtime = CLOCK_REALTIME;
const int clock_monotonic = CLOCK_REALTIME;
