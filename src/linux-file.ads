with Interfaces.C; use Interfaces.C;
with Interfaces; use Interfaces;

with Linux.Open;
with Linux.Rights;

package Linux.File is
   type Descriptor is new int range 0..int'Last;

   function Open
     (Path : String;
      Options : Linux.Open.Flags := Linux.Open.Read_Write;
      Create_Rights : Linux.Rights.Flags := Linux.Rights.No_Rights)
     return Descriptor;

   procedure Close (File_Descriptor : Descriptor);
end Linux.File;
