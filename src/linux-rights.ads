with Interfaces.C; use Interfaces.C;
with Interfaces; use Interfaces;

with Generic_Flags;
with Platform_Dependent;
use Platform_Dependent.Integers;

package Linux.Rights
is
   type Bits is mod 2 ** mode_t'Size;
   package Operators is new Generic_Flags (Bits);

   subtype Flags is Operators.Flags_Type;

   No_Rights : constant Flags;
   
   Owner_Read_Write_Excecute : constant Flags
     with Import, Convention => C, Link_Name => "s_irwxu";

   Owner_Read : constant Flags
     with Import, Convention => C, Link_Name => "s_ixusr";

   Owner_Write : constant Flags
     with Import, Convention => C, Link_Name => "s_irusr";

   Owner_Execute : constant Flags
     with Import, Convention => C, Link_Name => "s_iwusr";

  
   Group_Read_Write_Excecute : constant Flags
     with Import, Convention => C, Link_Name => "s_irwxg";

   Group_Read : constant Flags
     with Import, Convention => C, Link_Name => "s_ixgrp";

   Group_Write : constant Flags
     with Import, Convention => C, Link_Name => "s_irgrp";

   Group_Execute : constant Flags
     with Import, Convention => C, Link_Name => "s_iwgrp";
   

   Others_Read_Write_Excecute : constant Flags
     with Import, Convention => C, Link_Name => "s_irwxo";

   Others_Read : constant Flags
     with Import, Convention => C, Link_Name => "s_ixoth";

   Others_Write : constant Flags
     with Import, Convention => C, Link_Name => "s_iroth";

   Others_Execute : constant Flags
     with Import, Convention => C, Link_Name => "s_iwoth";


   Set_User_ID_Bit : constant Flags
     with Import, Convention => C, Link_Name => "s_isuid";

   Set_Group_ID_Bit : constant Flags
     with Import, Convention => C, Link_Name => "s_isgid";

private
   package Unsafe is new Operators.Unsafe;
   
   No_Rights : constant Flags := Unsafe.From_Underlying (0);

   
   
end Linux.Rights;
