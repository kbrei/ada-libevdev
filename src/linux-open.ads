with Interfaces.C; use Interfaces.C;
with Interfaces; use Interfaces;
with Generic_Flags;

package Linux.Open
is
   type Bits is mod 2 ** int'Size;
   package Operators is new Generic_Flags (Bits);

   subtype Flags is Operators.Flags_Type;

   No_Flags : constant Flags;

   Read_Only : constant Flags
     with Import, Convention => C, Link_Name => "o_rdonly";

   Write_Only : constant Flags
     with Import, Convention => C, Link_Name => "o_wronly";

   Read_Write : constant Flags
     with Import, Convention => C, Link_Name => "o_rdwr";

   Append : constant Flags
     with Import, Convention => C, Link_Name => "o_append";

   Async_Events : constant Flags
     with Import, Convention => C, Link_Name => "o_async";

   Close_On_Exec : constant Flags
     with Import, Convention => C, Link_Name => "o_cloexec";

   Create : constant Flags
     with Import, Convention => C, Link_Name => "o_creat";

   Directory : constant Flags
     with Import, Convention => C, Link_Name => "o_directory";

   Force_Create : constant Flags
     with Import, Convention => C, Link_Name => "o_excl";

   Dont_Control_TTY : constant Flags
     with Import, Convention => C, Link_Name => "o_noctty";

   Dont_Follow : constant Flags
     with Import, Convention => C, Link_Name => "o_nofollow";

   Dont_Block : constant Flags
     with Import, Convention => C, Link_Name => "o_nonblock";

   Sync_IO : constant Flags
     with Import, Convention => C, Link_Name => "o_sync";

   Data_Sync_IO : constant Flags
     with Import, Convention => C, Link_Name => "o_sync";

   Truncate : constant Flags
     with Import, Convention => C, Link_Name => "o_trunc";
private 
   package Unsafe is new Operators.Unsafe;

   No_Flags : constant Flags := Unsafe.From_Underlying (0);
end Linux.Open;
