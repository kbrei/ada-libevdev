with Interfaces.C; use Interfaces.C;
with Interfaces.C.Extensions; use Interfaces.C.Extensions;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with System;

-- for To_Event_Code
with Ada.Unchecked_Conversion;
with Ada.Real_Time; use Ada.Real_Time;

with Platform_Dependent; use Platform_Dependent.Integers;

package Libevdev.Types is
   -- Platform Independent integers

   -- however, Libevdev.Raw.Next_Event always returns int
   type Read_Mode_Underlying is new unsigned;

   type Event_Type_Underlying is new unsigned;
   type Event_Code is new unsigned;

   -- from libevdev.h
   -- struct libevdev *
   type Evdev is new System.Address;


   type Led_Value is (On, Off);
   for Led_Value'Size use Led_Value_Underlying'Size;
   for Led_Value use (On => 3, Off => 4);

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:806
   type Log_Priority is (Error, Info, Debug);
   for Log_Priority'Size use Log_Priority_Underlying'Size;
   for Log_Priority use (Error => 10, Info => 20, Debug => 30);

   type Var_Args is new System.Address;

   -- TODO: How to deal with va_List?
   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:901
   type Device_Log_Func is access procedure
     (Dev : Evdev;
      Priority : Log_Priority;
      Data : System.Address;
      File : chars_Ptr;
      Line : int;
      Func : chars_Ptr;
      Format : chars_Ptr;
      Args : Var_Args)
   with Convention => C;

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:938
   type Grab_Mode is (Grab, Ungrab);
   for Grab_Mode'Size use Grab_Mode_Underlying'Size;
   for Grab_Mode use (Grab => 3, Ungrab => 4);

   type Property is
     (Pointer,
      Direct,
      Buttonpad,
      Semi_MT,
      Top_Buttonpad,
      Pointing_Stick,
      Accelerometer);
   for Property'Size use Unsigned'Size;
   for Property use
     (Pointer => 16#00#,
      Direct =>  16#01#,
      Buttonpad => 16#02#,
      Semi_Mt => 16#03#,
      Top_Buttonpad => 16#04#,
      Pointing_Stick => 16#05#,
      Accelerometer => 16#06#);

   -- TODO: what about force feedback?
   -- TODO: what about PWR?

   type Event_Type is
     (Synchronization,
      Key_And_Button,
      Relative_Axis,
      Absolute_Axis,
      Misc,
      Switch,
      LED,
      Sound,
      Autorepeat);
   for Event_Type'Size use Event_Type_Underlying'Size;
   for event_Type use
     (Synchronization => 16#00#,
      Key_And_Button => 16#01#,
      Relative_Axis => 16#02#,
      Absolute_Axis => 16#03#,
      Misc => 16#04#,
      Switch => 16#05#,
      Led => 16#11#,
      Sound => 16#12#,
      Autorepeat => 16#14#);

   type Synchronization_Code is
     (Report,
      Config,
      Mt_Report,
      Dropped);
   for Synchronization_Code'Size use Event_Code'Size;
   for Synchronization_Code use
     (Report => 0,
      Config => 1,
      Mt_Report => 2,
      Dropped => 3);

   type Key_And_Button_Code is
     (Key_Reserved,
      Key_Esc,
      Key_1,
      Key_2,
      Key_3,
      Key_4,
      Key_5,
      Key_6,
      Key_7,
      Key_8,
      Key_9,
      Key_0,
      Key_Minus,
      Key_Equal,
      Key_Backspace,
      Key_Tab,
      Key_Q,
      Key_W,
      Key_E,
      Key_R,
      Key_T,
      Key_Y,
      Key_U,
      Key_I,
      Key_O,
      Key_P,
      Key_Leftbrace,
      Key_Rightbrace,
      Key_Enter,
      Key_Leftctrl,
      Key_A,
      Key_S,
      Key_D,
      Key_F,
      Key_G,
      Key_H,
      Key_J,
      Key_K,
      Key_L,
      Key_Semicolon,
      Key_Apostrophe,
      Key_Grave,
      Key_Leftshift,
      Key_Backslash,
      Key_Z,
      Key_X,
      Key_C,
      Key_V,
      Key_B,
      Key_N,
      Key_M,
      Key_Comma,
      Key_Dot,
      Key_Slash,
      Key_Rightshift,
      Key_Kpasterisk,
      Key_Leftalt,
      Key_Space,
      Key_Capslock,
      Key_F1,
      Key_F2,
      Key_F3,
      Key_F4,
      Key_F5,
      Key_F6,
      Key_F7,
      Key_F8,
      Key_F9,
      Key_F10,
      Key_Numlock,
      Key_Scrolllock,
      Key_Kp7,
      Key_Kp8,
      Key_Kp9,
      Key_Kpminus,
      Key_Kp4,
      Key_Kp5,
      Key_Kp6,
      Key_Kpplus,
      Key_Kp1,
      Key_Kp2,
      Key_Kp3,
      Key_Kp0,
      Key_Kpdot,
      Key_Zenkakuhankaku,
      Key_102nd,
      Key_F11,
      Key_F12,
      Key_Ro,
      Key_Katakana,
      Key_Hiragana,
      Key_Henkan,
      Key_Katakanahiragana,
      Key_Muhenkan,
      Key_Kpjpcomma,
      Key_Kpenter,
      Key_Rightctrl,
      Key_Kpslash,
      Key_Sysrq,
      Key_Rightalt,
      Key_Linefeed,
      Key_Home,
      Key_Up,
      Key_Pageup,
      Key_Left,
      Key_Right,
      Key_End,
      Key_Down,
      Key_Pagedown,
      Key_Insert,
      Key_Delete,
      Key_Macro,
      Key_Mute,
      Key_Volumedown,
      Key_Volumeup,
      Key_Power,
      Key_Kpequal,
      Key_Kpplusminus,
      Key_Pause,
      Key_Scale,
      Key_Kpcomma,
      Key_Hangeul,
      Key_Hanja,
      Key_Yen,
      Key_Leftmeta,
      Key_Rightmeta,
      Key_Compose,
      Key_Stop,
      Key_Again,
      Key_Props,
      Key_Undo,
      Key_Front,
      Key_Copy,
      Key_Open,
      Key_Paste,
      Key_Find,
      Key_Cut,
      Key_Help,
      Key_Menu,
      Key_Calc,
      Key_Setup,
      Key_Sleep,
      Key_Wakeup,
      Key_File,
      Key_Sendfile,
      Key_Deletefile,
      Key_Xfer,
      Key_Prog1,
      Key_Prog2,
      Key_Www,
      Key_Msdos,
      Key_Coffee,
      Key_Direction,
      Key_Cyclewindows,
      Key_Mail,
      Key_Bookmarks,
      Key_Computer,
      Key_Back,
      Key_Forward,
      Key_Closecd,
      Key_Ejectcd,
      Key_Ejectclosecd,
      Key_Nextsong,
      Key_Playpause,
      Key_Previoussong,
      Key_Stopcd,
      Key_Record,
      Key_Rewind,
      Key_Phone,
      Key_Iso,
      Key_Config,
      Key_Homepage,
      Key_Refresh,
      Key_Exit,
      Key_Move,
      Key_Edit,
      Key_Scrollup,
      Key_Scrolldown,
      Key_Kpleftparen,
      Key_Kprightparen,
      Key_New,
      Key_Redo,
      Key_F13,
      Key_F14,
      Key_F15,
      Key_F16,
      Key_F17,
      Key_F18,
      Key_F19,
      Key_F20,
      Key_F21,
      Key_F22,
      Key_F23,
      Key_F24,
      Key_Playcd,
      Key_Pausecd,
      Key_Prog3,
      Key_Prog4,
      Key_Dashboard,
      Key_Suspend,
      Key_Close,
      Key_Play,
      Key_Fastforward,
      Key_Bassboost,
      Key_Print,
      Key_Hp,
      Key_Camera,
      Key_Sound,
      Key_Question,
      Key_Email,
      Key_Chat,
      Key_Search,
      Key_Connect,
      Key_Finance,
      Key_Sport,
      Key_Shop,
      Key_Alterase,
      Key_Cancel,
      Key_Brightnessdown,
      Key_Brightnessup,
      Key_Media,
      Key_Switchvideomode,
      Key_Kbdillumtoggle,
      Key_Kbdillumdown,
      Key_Kbdillumup,
      Key_Send,
      Key_Reply,
      Key_Forwardmail,
      Key_Save,
      Key_Documents,
      Key_Battery,
      Key_Bluetooth,
      Key_Wlan,
      Key_Uwb,
      Key_Unknown,
      Key_Video_Next,
      Key_Video_Prev,
      Key_Brightness_Cycle,
      Key_Brightness_Auto,
      Key_Display_Off,
      Key_Wwan,
      Key_Rfkill,
      Key_Micmute,
      Button_0,
      Button_1,
      Button_2,
      Button_3,
      Button_4,
      Button_5,
      Button_6,
      Button_7,
      Button_8,
      Button_9,
      Button_Left,
      Button_Right,
      Button_Middle,
      Button_Side,
      Button_Extra,
      Button_Forward,
      Button_Back,
      Button_Task,
      Button_Joystick,
      Button_Thumb,
      Button_Thumb2,
      Button_Top,
      Button_Top2,
      Button_Pinkie,
      Button_Base,
      Button_Base2,
      Button_Base3,
      Button_Base4,
      Button_Base5,
      Button_Base6,
      Button_Dead,
      Button_South,
      Button_East,
      Button_C,
      Button_North,
      Button_West,
      Button_Z,
      Button_Tl,
      Button_Tr,
      Button_Tl2,
      Button_Tr2,
      Button_Select,
      Button_Start,
      Button_Mode,
      Button_Thumbl,
      Button_Thumbr,
      Button_Tool_Pen,
      Button_Tool_Rubber,
      Button_Tool_Brush,
      Button_Tool_Pencil,
      Button_Tool_Airbrush,
      Button_Tool_Finger,
      Button_Tool_Mouse,
      Button_Tool_Lens,
      Button_Tool_Quinttap,
      Button_Touch,
      Button_Stylus,
      Button_Stylus2,
      Button_Tool_Doubletap,
      Button_Tool_Tripletap,
      Button_Tool_Quadtap,
      Button_Gear_Down,
      Button_Gear_Up,
      Key_Ok,
      Key_Select,
      Key_Goto,
      Key_Clear,
      Key_Power2,
      Key_Option,
      Key_Info,
      Key_Time,
      Key_Vendor,
      Key_Archive,
      Key_Program,
      Key_Channel,
      Key_Favorites,
      Key_Epg,
      Key_Pvr,
      Key_Mhp,
      Key_Language,
      Key_Title,
      Key_Subtitle,
      Key_Angle,
      Key_Zoom,
      Key_Mode,
      Key_Keyboard,
      Key_Screen,
      Key_Pc,
      Key_Tv,
      Key_Tv2,
      Key_Vcr,
      Key_Vcr2,
      Key_Sat,
      Key_Sat2,
      Key_Cd,
      Key_Tape,
      Key_Radio,
      Key_Tuner,
      Key_Player,
      Key_Text,
      Key_Dvd,
      Key_Aux,
      Key_Mp3,
      Key_Audio,
      Key_Video,
      Key_Directory,
      Key_List,
      Key_Memo,
      Key_Calendar,
      Key_Red,
      Key_Green,
      Key_Yellow,
      Key_Blue,
      Key_Channelup,
      Key_Channeldown,
      Key_First,
      Key_Last,
      Key_Ab,
      Key_Next,
      Key_Restart,
      Key_Slow,
      Key_Shuffle,
      Key_Break,
      Key_Previous,
      Key_Digits,
      Key_Teen,
      Key_Twen,
      Key_Videophone,
      Key_Games,
      Key_Zoomin,
      Key_Zoomout,
      Key_Zoomreset,
      Key_Wordprocessor,
      Key_Editor,
      Key_Spreadsheet,
      Key_Graphicseditor,
      Key_Presentation,
      Key_Database,
      Key_News,
      Key_Voicemail,
      Key_Addressbook,
      Key_Messenger,
      Key_Displaytoggle,
      Key_Spellcheck,
      Key_Logoff,
      Key_Dollar,
      Key_Euro,
      Key_Frameback,
      Key_Frameforward,
      Key_Context_Menu,
      Key_Media_Repeat,
      Key_10channelsup,
      Key_10channelsdown,
      Key_Images,
      Key_Del_Eol,
      Key_Del_Eos,
      Key_Ins_Line,
      Key_Del_Line,
      Key_Fn,
      Key_Fn_Esc,
      Key_Fn_F1,
      Key_Fn_F2,
      Key_Fn_F3,
      Key_Fn_F4,
      Key_Fn_F5,
      Key_Fn_F6,
      Key_Fn_F7,
      Key_Fn_F8,
      Key_Fn_F9,
      Key_Fn_F10,
      Key_Fn_F11,
      Key_Fn_F12,
      Key_Fn_1,
      Key_Fn_2,
      Key_Fn_D,
      Key_Fn_E,
      Key_Fn_F,
      Key_Fn_S,
      Key_Fn_B,
      Key_Brl_Dot1,
      Key_Brl_Dot2,
      Key_Brl_Dot3,
      Key_Brl_Dot4,
      Key_Brl_Dot5,
      Key_Brl_Dot6,
      Key_Brl_Dot7,
      Key_Brl_Dot8,
      Key_Brl_Dot9,
      Key_Brl_Dot10,
      Key_Numeric_0,
      Key_Numeric_1,
      Key_Numeric_2,
      Key_Numeric_3,
      Key_Numeric_4,
      Key_Numeric_5,
      Key_Numeric_6,
      Key_Numeric_7,
      Key_Numeric_8,
      Key_Numeric_9,
      Key_Numeric_Star,
      Key_Numeric_Pound,
      Key_Camera_Focus,
      Key_Wps_Button,
      Key_Touchpad_Toggle,
      Key_Touchpad_On,
      Key_Touchpad_Off,
      Key_Camera_Zoomin,
      Key_Camera_Zoomout,
      Key_Camera_Up,
      Key_Camera_Down,
      Key_Camera_Left,
      Key_Camera_Right,
      Key_Attendant_On,
      Key_Attendant_Off,
      Key_Attendant_Toggle,
      Key_Lights_Toggle,
      Button_Dpad_Up,
      Button_Dpad_Down,
      Button_Dpad_Left,
      Button_Dpad_Right,
      Key_Als_Toggle,
      Key_Buttonconfig,
      Key_Taskmanager,
      Key_Journal,
      Key_Controlpanel,
      Key_Appselect,
      Key_Screensaver,
      Key_Voicecommand,
      Key_Brightness_Min,
      Key_Kbdinputassist_Prev,
      Key_Kbdinputassist_Next,
      Key_Kbdinputassist_Prevgroup,
      Key_Kbdinputassist_Nextgroup,
      Key_Kbdinputassist_Accept,
      Key_Kbdinputassist_Cancel,
      Button_Trigger_Happy1,
      Button_Trigger_Happy2,
      Button_Trigger_Happy3,
      Button_Trigger_Happy4,
      Button_Trigger_Happy5,
      Button_Trigger_Happy6,
      Button_Trigger_Happy7,
      Button_Trigger_Happy8,
      Button_Trigger_Happy9,
      Button_Trigger_Happy10,
      Button_Trigger_Happy11,
      Button_Trigger_Happy12,
      Button_Trigger_Happy13,
      Button_Trigger_Happy14,
      Button_Trigger_Happy15,
      Button_Trigger_Happy16,
      Button_Trigger_Happy17,
      Button_Trigger_Happy18,
      Button_Trigger_Happy19,
      Button_Trigger_Happy20,
      Button_Trigger_Happy21,
      Button_Trigger_Happy22,
      Button_Trigger_Happy23,
      Button_Trigger_Happy24,
      Button_Trigger_Happy25,
      Button_Trigger_Happy26,
      Button_Trigger_Happy27,
      Button_Trigger_Happy28,
      Button_Trigger_Happy29,
      Button_Trigger_Happy30,
      Button_Trigger_Happy31,
      Button_Trigger_Happy32,
      Button_Trigger_Happy33,
      Button_Trigger_Happy34,
      Button_Trigger_Happy35,
      Button_Trigger_Happy36,
      Button_Trigger_Happy37,
      Button_Trigger_Happy38,
      Button_Trigger_Happy39,
      Button_Trigger_Happy40);
   for Key_And_Button_Code'Size use Event_Code'Size;
   for Key_And_Button_Code use
     (Key_Reserved => 0,
      Key_Esc => 1,
      Key_1 => 2,
      Key_2 => 3,
      Key_3 => 4,
      Key_4 => 5,
      Key_5 => 6,
      Key_6 => 7,
      Key_7 => 8,
      Key_8 => 9,
      Key_9 => 10,
      Key_0 => 11,
      Key_Minus => 12,
      Key_Equal => 13,
      Key_Backspace => 14,
      Key_Tab => 15,
      Key_Q => 16,
      Key_W => 17,
      Key_E => 18,
      Key_R => 19,
      Key_T => 20,
      Key_Y => 21,
      Key_U => 22,
      Key_I => 23,
      Key_O => 24,
      Key_P => 25,
      Key_Leftbrace => 26,
      Key_Rightbrace => 27,
      Key_Enter => 28,
      Key_Leftctrl => 29,
      Key_A => 30,
      Key_S => 31,
      Key_D => 32,
      Key_F => 33,
      Key_G => 34,
      Key_H => 35,
      Key_J => 36,
      Key_K => 37,
      Key_L => 38,
      Key_Semicolon => 39,
      Key_Apostrophe => 40,
      Key_Grave => 41,
      Key_Leftshift => 42,
      Key_Backslash => 43,
      Key_Z => 44,
      Key_X => 45,
      Key_C => 46,
      Key_V => 47,
      Key_B => 48,
      Key_N => 49,
      Key_M => 50,
      Key_Comma => 51,
      Key_Dot => 52,
      Key_Slash => 53,
      Key_Rightshift => 54,
      Key_Kpasterisk => 55,
      Key_Leftalt => 56,
      Key_Space => 57,
      Key_Capslock => 58,
      Key_F1 => 59,
      Key_F2 => 60,
      Key_F3 => 61,
      Key_F4 => 62,
      Key_F5 => 63,
      Key_F6 => 64,
      Key_F7 => 65,
      Key_F8 => 66,
      Key_F9 => 67,
      Key_F10 => 68,
      Key_Numlock => 69,
      Key_Scrolllock => 70,
      Key_Kp7 => 71,
      Key_Kp8 => 72,
      Key_Kp9 => 73,
      Key_Kpminus => 74,
      Key_Kp4 => 75,
      Key_Kp5 => 76,
      Key_Kp6 => 77,
      Key_Kpplus => 78,
      Key_Kp1 => 79,
      Key_Kp2 => 80,
      Key_Kp3 => 81,
      Key_Kp0 => 82,
      Key_Kpdot => 83,
      Key_Zenkakuhankaku => 85,
      Key_102nd => 86,
      Key_F11 => 87,
      Key_F12 => 88,
      Key_Ro => 89,
      Key_Katakana => 90,
      Key_Hiragana => 91,
      Key_Henkan => 92,
      Key_Katakanahiragana => 93,
      Key_Muhenkan => 94,
      Key_Kpjpcomma => 95,
      Key_Kpenter => 96,
      Key_Rightctrl => 97,
      Key_Kpslash => 98,
      Key_Sysrq => 99,
      Key_Rightalt => 100,
      Key_Linefeed => 101,
      Key_Home => 102,
      Key_Up => 103,
      Key_Pageup => 104,
      Key_Left => 105,
      Key_Right => 106,
      Key_End => 107,
      Key_Down => 108,
      Key_Pagedown => 109,
      Key_Insert => 110,
      Key_Delete => 111,
      Key_Macro => 112,
      Key_Mute => 113,
      Key_Volumedown => 114,
      Key_Volumeup => 115,
      Key_Power => 116,
      Key_Kpequal => 117,
      Key_Kpplusminus => 118,
      Key_Pause => 119,
      Key_Scale => 120,
      Key_Kpcomma => 121,
      Key_Hangeul => 122,
      Key_Hanja => 123,
      Key_Yen => 124,
      Key_Leftmeta => 125,
      Key_Rightmeta => 126,
      Key_Compose => 127,
      Key_Stop => 128,
      Key_Again => 129,
      Key_Props => 130,
      Key_Undo => 131,
      Key_Front => 132,
      Key_Copy => 133,
      Key_Open => 134,
      Key_Paste => 135,
      Key_Find => 136,
      Key_Cut => 137,
      Key_Help => 138,
      Key_Menu => 139,
      Key_Calc => 140,
      Key_Setup => 141,
      Key_Sleep => 142,
      Key_Wakeup => 143,
      Key_File => 144,
      Key_Sendfile => 145,
      Key_Deletefile => 146,
      Key_Xfer => 147,
      Key_Prog1 => 148,
      Key_Prog2 => 149,
      Key_Www => 150,
      Key_Msdos => 151,
      Key_Coffee => 152,
      Key_Direction => 153,
      Key_Cyclewindows => 154,
      Key_Mail => 155,
      Key_Bookmarks => 156,
      Key_Computer => 157,
      Key_Back => 158,
      Key_Forward => 159,
      Key_Closecd => 160,
      Key_Ejectcd => 161,
      Key_Ejectclosecd => 162,
      Key_Nextsong => 163,
      Key_Playpause => 164,
      Key_Previoussong => 165,
      Key_Stopcd => 166,
      Key_Record => 167,
      Key_Rewind => 168,
      Key_Phone => 169,
      Key_Iso => 170,
      Key_Config => 171,
      Key_Homepage => 172,
      Key_Refresh => 173,
      Key_Exit => 174,
      Key_Move => 175,
      Key_Edit => 176,
      Key_Scrollup => 177,
      Key_Scrolldown => 178,
      Key_Kpleftparen => 179,
      Key_Kprightparen => 180,
      Key_New => 181,
      Key_Redo => 182,
      Key_F13 => 183,
      Key_F14 => 184,
      Key_F15 => 185,
      Key_F16 => 186,
      Key_F17 => 187,
      Key_F18 => 188,
      Key_F19 => 189,
      Key_F20 => 190,
      Key_F21 => 191,
      Key_F22 => 192,
      Key_F23 => 193,
      Key_F24 => 194,
      Key_Playcd => 200,
      Key_Pausecd => 201,
      Key_Prog3 => 202,
      Key_Prog4 => 203,
      Key_Dashboard => 204,
      Key_Suspend => 205,
      Key_Close => 206,
      Key_Play => 207,
      Key_Fastforward => 208,
      Key_Bassboost => 209,
      Key_Print => 210,
      Key_Hp => 211,
      Key_Camera => 212,
      Key_Sound => 213,
      Key_Question => 214,
      Key_Email => 215,
      Key_Chat => 216,
      Key_Search => 217,
      Key_Connect => 218,
      Key_Finance => 219,
      Key_Sport => 220,
      Key_Shop => 221,
      Key_Alterase => 222,
      Key_Cancel => 223,
      Key_Brightnessdown => 224,
      Key_Brightnessup => 225,
      Key_Media => 226,
      Key_Switchvideomode => 227,
      Key_Kbdillumtoggle => 228,
      Key_Kbdillumdown => 229,
      Key_Kbdillumup => 230,
      Key_Send => 231,
      Key_Reply => 232,
      Key_Forwardmail => 233,
      Key_Save => 234,
      Key_Documents => 235,
      Key_Battery => 236,
      Key_Bluetooth => 237,
      Key_Wlan => 238,
      Key_Uwb => 239,
      Key_Unknown => 240,
      Key_Video_Next => 241,
      Key_Video_Prev => 242,
      Key_Brightness_Cycle => 243,
      Key_Brightness_Auto => 244,
      Key_Display_Off => 245,
      Key_Wwan => 246,
      Key_Rfkill => 247,
      Key_Micmute => 248,
      Button_0 => 16#100#,
      Button_1 => 16#101#,
      Button_2 => 16#102#,
      Button_3 => 16#103#,
      Button_4 => 16#104#,
      Button_5 => 16#105#,
      Button_6 => 16#106#,
      Button_7 => 16#107#,
      Button_8 => 16#108#,
      Button_9 => 16#109#,
      Button_Left => 16#110#,
      Button_Right => 16#111#,
      Button_Middle => 16#112#,
      Button_Side => 16#113#,
      Button_Extra => 16#114#,
      Button_Forward => 16#115#,
      Button_Back => 16#116#,
      Button_Task => 16#117#,
      Button_Joystick => 16#120#,
      Button_Thumb => 16#121#,
      Button_Thumb2 => 16#122#,
      Button_Top => 16#123#,
      Button_Top2 => 16#124#,
      Button_Pinkie => 16#125#,
      Button_Base => 16#126#,
      Button_Base2 => 16#127#,
      Button_Base3 => 16#128#,
      Button_Base4 => 16#129#,
      Button_Base5 => 16#12a#,
      Button_Base6 => 16#12b#,
      Button_Dead => 16#12f#,
      Button_South => 16#130#,
      Button_East => 16#131#,
      Button_C => 16#132#,
      Button_North => 16#133#,
      Button_West => 16#134#,
      Button_Z => 16#135#,
      Button_Tl => 16#136#,
      Button_Tr => 16#137#,
      Button_Tl2 => 16#138#,
      Button_Tr2 => 16#139#,
      Button_Select => 16#13a#,
      Button_Start => 16#13b#,
      Button_Mode => 16#13c#,
      Button_Thumbl => 16#13d#,
      Button_Thumbr => 16#13e#,
      Button_Tool_Pen => 16#140#,
      Button_Tool_Rubber => 16#141#,
      Button_Tool_Brush => 16#142#,
      Button_Tool_Pencil => 16#143#,
      Button_Tool_Airbrush => 16#144#,
      Button_Tool_Finger => 16#145#,
      Button_Tool_Mouse => 16#146#,
      Button_Tool_Lens => 16#147#,
      Button_Tool_Quinttap => 16#148#,
      Button_Touch => 16#14a#,
      Button_Stylus => 16#14b#,
      Button_Stylus2 => 16#14c#,
      Button_Tool_Doubletap => 16#14d#,
      Button_Tool_Tripletap => 16#14e#,
      Button_Tool_Quadtap => 16#14f#,
      Button_Gear_Down => 16#150#,
      Button_Gear_Up => 16#151#,
      Key_Ok => 16#160#,
      Key_Select => 16#161#,
      Key_Goto => 16#162#,
      Key_Clear => 16#163#,
      Key_Power2 => 16#164#,
      Key_Option => 16#165#,
      Key_Info => 16#166#,
      Key_Time => 16#167#,
      Key_Vendor => 16#168#,
      Key_Archive => 16#169#,
      Key_Program => 16#16a#,
      Key_Channel => 16#16b#,
      Key_Favorites => 16#16c#,
      Key_Epg => 16#16d#,
      Key_Pvr => 16#16e#,
      Key_Mhp => 16#16f#,
      Key_Language => 16#170#,
      Key_Title => 16#171#,
      Key_Subtitle => 16#172#,
      Key_Angle => 16#173#,
      Key_Zoom => 16#174#,
      Key_Mode => 16#175#,
      Key_Keyboard => 16#176#,
      Key_Screen => 16#177#,
      Key_Pc => 16#178#,
      Key_Tv => 16#179#,
      Key_Tv2 => 16#17a#,
      Key_Vcr => 16#17b#,
      Key_Vcr2 => 16#17c#,
      Key_Sat => 16#17d#,
      Key_Sat2 => 16#17e#,
      Key_Cd => 16#17f#,
      Key_Tape => 16#180#,
      Key_Radio => 16#181#,
      Key_Tuner => 16#182#,
      Key_Player => 16#183#,
      Key_Text => 16#184#,
      Key_Dvd => 16#185#,
      Key_Aux => 16#186#,
      Key_Mp3 => 16#187#,
      Key_Audio => 16#188#,
      Key_Video => 16#189#,
      Key_Directory => 16#18a#,
      Key_List => 16#18b#,
      Key_Memo => 16#18c#,
      Key_Calendar => 16#18d#,
      Key_Red => 16#18e#,
      Key_Green => 16#18f#,
      Key_Yellow => 16#190#,
      Key_Blue => 16#191#,
      Key_Channelup => 16#192#,
      Key_Channeldown => 16#193#,
      Key_First => 16#194#,
      Key_Last => 16#195#,
      Key_Ab => 16#196#,
      Key_Next => 16#197#,
      Key_Restart => 16#198#,
      Key_Slow => 16#199#,
      Key_Shuffle => 16#19a#,
      Key_Break => 16#19b#,
      Key_Previous => 16#19c#,
      Key_Digits => 16#19d#,
      Key_Teen => 16#19e#,
      Key_Twen => 16#19f#,
      Key_Videophone => 16#1a0#,
      Key_Games => 16#1a1#,
      Key_Zoomin => 16#1a2#,
      Key_Zoomout => 16#1a3#,
      Key_Zoomreset => 16#1a4#,
      Key_Wordprocessor => 16#1a5#,
      Key_Editor => 16#1a6#,
      Key_Spreadsheet => 16#1a7#,
      Key_Graphicseditor => 16#1a8#,
      Key_Presentation => 16#1a9#,
      Key_Database => 16#1aa#,
      Key_News => 16#1ab#,
      Key_Voicemail => 16#1ac#,
      Key_Addressbook => 16#1ad#,
      Key_Messenger => 16#1ae#,
      Key_Displaytoggle => 16#1af#,
      Key_Spellcheck => 16#1b0#,
      Key_Logoff => 16#1b1#,
      Key_Dollar => 16#1b2#,
      Key_Euro => 16#1b3#,
      Key_Frameback => 16#1b4#,
      Key_Frameforward => 16#1b5#,
      Key_Context_Menu => 16#1b6#,
      Key_Media_Repeat => 16#1b7#,
      Key_10channelsup => 16#1b8#,
      Key_10channelsdown => 16#1b9#,
      Key_Images => 16#1ba#,
      Key_Del_Eol => 16#1c0#,
      Key_Del_Eos => 16#1c1#,
      Key_Ins_Line => 16#1c2#,
      Key_Del_Line => 16#1c3#,
      Key_Fn => 16#1d0#,
      Key_Fn_Esc => 16#1d1#,
      Key_Fn_F1 => 16#1d2#,
      Key_Fn_F2 => 16#1d3#,
      Key_Fn_F3 => 16#1d4#,
      Key_Fn_F4 => 16#1d5#,
      Key_Fn_F5 => 16#1d6#,
      Key_Fn_F6 => 16#1d7#,
      Key_Fn_F7 => 16#1d8#,
      Key_Fn_F8 => 16#1d9#,
      Key_Fn_F9 => 16#1da#,
      Key_Fn_F10 => 16#1db#,
      Key_Fn_F11 => 16#1dc#,
      Key_Fn_F12 => 16#1dd#,
      Key_Fn_1 => 16#1de#,
      Key_Fn_2 => 16#1df#,
      Key_Fn_D => 16#1e0#,
      Key_Fn_E => 16#1e1#,
      Key_Fn_F => 16#1e2#,
      Key_Fn_S => 16#1e3#,
      Key_Fn_B => 16#1e4#,
      Key_Brl_Dot1 => 16#1f1#,
      Key_Brl_Dot2 => 16#1f2#,
      Key_Brl_Dot3 => 16#1f3#,
      Key_Brl_Dot4 => 16#1f4#,
      Key_Brl_Dot5 => 16#1f5#,
      Key_Brl_Dot6 => 16#1f6#,
      Key_Brl_Dot7 => 16#1f7#,
      Key_Brl_Dot8 => 16#1f8#,
      Key_Brl_Dot9 => 16#1f9#,
      Key_Brl_Dot10 => 16#1fa#,
      Key_Numeric_0 => 16#200#,
      Key_Numeric_1 => 16#201#,
      Key_Numeric_2 => 16#202#,
      Key_Numeric_3 => 16#203#,
      Key_Numeric_4 => 16#204#,
      Key_Numeric_5 => 16#205#,
      Key_Numeric_6 => 16#206#,
      Key_Numeric_7 => 16#207#,
      Key_Numeric_8 => 16#208#,
      Key_Numeric_9 => 16#209#,
      Key_Numeric_Star => 16#20a#,
      Key_Numeric_Pound => 16#20b#,
      Key_Camera_Focus => 16#210#,
      Key_Wps_Button => 16#211#,
      Key_Touchpad_Toggle => 16#212#,
      Key_Touchpad_On => 16#213#,
      Key_Touchpad_Off => 16#214#,
      Key_Camera_Zoomin => 16#215#,
      Key_Camera_Zoomout => 16#216#,
      Key_Camera_Up => 16#217#,
      Key_Camera_Down => 16#218#,
      Key_Camera_Left => 16#219#,
      Key_Camera_Right => 16#21a#,
      Key_Attendant_On => 16#21b#,
      Key_Attendant_Off => 16#21c#,
      Key_Attendant_Toggle => 16#21d#,
      Key_Lights_Toggle => 16#21e#,
      Button_Dpad_Up => 16#220#,
      Button_Dpad_Down => 16#221#,
      Button_Dpad_Left => 16#222#,
      Button_Dpad_Right => 16#223#,
      Key_Als_Toggle => 16#230#,
      Key_Buttonconfig => 16#240#,
      Key_Taskmanager => 16#241#,
      Key_Journal => 16#242#,
      Key_Controlpanel => 16#243#,
      Key_Appselect => 16#244#,
      Key_Screensaver => 16#245#,
      Key_Voicecommand => 16#246#,
      Key_Brightness_Min => 16#250#,
      Key_Kbdinputassist_Prev => 16#260#,
      Key_Kbdinputassist_Next => 16#261#,
      Key_Kbdinputassist_Prevgroup => 16#262#,
      Key_Kbdinputassist_Nextgroup => 16#263#,
      Key_Kbdinputassist_Accept => 16#264#,
      Key_Kbdinputassist_Cancel => 16#265#,
      Button_Trigger_Happy1 => 16#2c0#,
      Button_Trigger_Happy2 => 16#2c1#,
      Button_Trigger_Happy3 => 16#2c2#,
      Button_Trigger_Happy4 => 16#2c3#,
      Button_Trigger_Happy5 => 16#2c4#,
      Button_Trigger_Happy6 => 16#2c5#,
      Button_Trigger_Happy7 => 16#2c6#,
      Button_Trigger_Happy8 => 16#2c7#,
      Button_Trigger_Happy9 => 16#2c8#,
      Button_Trigger_Happy10 => 16#2c9#,
      Button_Trigger_Happy11 => 16#2ca#,
      Button_Trigger_Happy12 => 16#2cb#,
      Button_Trigger_Happy13 => 16#2cc#,
      Button_Trigger_Happy14 => 16#2cd#,
      Button_Trigger_Happy15 => 16#2ce#,
      Button_Trigger_Happy16 => 16#2cf#,
      Button_Trigger_Happy17 => 16#2d0#,
      Button_Trigger_Happy18 => 16#2d1#,
      Button_Trigger_Happy19 => 16#2d2#,
      Button_Trigger_Happy20 => 16#2d3#,
      Button_Trigger_Happy21 => 16#2d4#,
      Button_Trigger_Happy22 => 16#2d5#,
      Button_Trigger_Happy23 => 16#2d6#,
      Button_Trigger_Happy24 => 16#2d7#,
      Button_Trigger_Happy25 => 16#2d8#,
      Button_Trigger_Happy26 => 16#2d9#,
      Button_Trigger_Happy27 => 16#2da#,
      Button_Trigger_Happy28 => 16#2db#,
      Button_Trigger_Happy29 => 16#2dc#,
      Button_Trigger_Happy30 => 16#2dd#,
      Button_Trigger_Happy31 => 16#2de#,
      Button_Trigger_Happy32 => 16#2df#,
      Button_Trigger_Happy33 => 16#2e0#,
      Button_Trigger_Happy34 => 16#2e1#,
      Button_Trigger_Happy35 => 16#2e2#,
      Button_Trigger_Happy36 => 16#2e3#,
      Button_Trigger_Happy37 => 16#2e4#,
      Button_Trigger_Happy38 => 16#2e5#,
      Button_Trigger_Happy39 => 16#2e6#,
      Button_Trigger_Happy40 => 16#2e7#);

   -- key and button aliases
   Key_Hanguel : constant Key_And_Button_Code := Key_Hangeul;
   Key_Screenlock : constant Key_And_Button_Code := Key_Coffee;
   Key_Brightness_Zero : constant Key_And_Button_Code := Key_Brightness_Auto;
   Key_Wimax : constant Key_And_Button_Code := key_Wwan;
   Key_Brightness_Toggle : constant Key_And_Button_Code := key_Displaytoggle;
   Key_Min_Interesting : constant Key_And_Button_Code := Key_Mute;
   Button_A : constant Key_And_Button_Code := Button_South;
   Button_B : constant Key_And_Button_Code := Button_East;
   Button_X : constant Key_And_Button_Code := Button_North;
   Button_Y : constant Key_And_Button_Code := Button_West;
   Button_Misc : constant Key_And_Button_Code := Button_0;
   Button_Trigger : constant Key_And_Button_Code := Button_Joystick;
   Button_Mouse : constant Key_And_Button_Code := Button_Left;
   Button_Trigger_Happy : constant Key_And_Button_Code := Button_Trigger_Happy1;
   Button_Wheel : constant Key_And_Button_Code := Button_Gear_Down;
   Button_Digi : constant Key_And_Button_Code := Button_Tool_Pen;
   Button_Gamepad : constant Key_And_Button_Code := Button_South;

   type Relative_Axis_Code is
     (X,
      Y,
      Z,
      Rx,
      Ry,
      Rz,
      Hwheel,
      Dial,
      Wheel,
      Misc);
   for Relative_Axis_Code'Size use Event_Code'Size;
   for Relative_Axis_Code use
     (X => 16#00#,
      Y => 16#01#,
      Z => 16#02#,
      Rx => 16#03#,
      Ry => 16#04#,
      Rz => 16#05#,
      Hwheel => 16#06#,
      Dial => 16#07#,
      Wheel => 16#08#,
      Misc => 16#09#);

   type Absolute_Axis_Code is
     (X,
      Y,
      Z,
      Rx,
      Ry,
      Rz,
      Throttle,
      Rudder,
      Wheel,
      Gas,
      Brake,
      Hat0x,
      Hat0y,
      Hat1x,
      Hat1y,
      Hat2x,
      Hat2y,
      Hat3x,
      Hat3y,
      Pressure,
      Distance,
      Tilt_X,
      Tilt_Y,
      Tool_Width,
      Volume,
      Misc,
      MT_Slot,
      MT_Touch_Major,
      MT_Touch_Minor,
      MT_Width_Major,
      MT_Width_Minor,
      MT_Orientation,
      MT_Position_X,
      MT_Position_Y,
      MT_Tool_Type,
      MT_Blob_Id,
      MT_Tracking_Id,
      MT_Pressure,
      MT_Distance,
      MT_Tool_X,
      MT_Tool_Y);
   for Absolute_Axis_Code'Size use Event_Code'Size;
   for Absolute_Axis_Code use
     (X => 16#00#,
      Y => 16#01#,
      Z => 16#02#,
      Rx => 16#03#,
      Ry => 16#04#,
      Rz => 16#05#,
      Throttle => 16#06#,
      Rudder => 16#07#,
      Wheel => 16#08#,
      Gas => 16#09#,
      Brake => 16#0a#,
      Hat0x => 16#10#,
      Hat0y => 16#11#,
      Hat1x => 16#12#,
      Hat1y => 16#13#,
      Hat2x => 16#14#,
      Hat2y => 16#15#,
      Hat3x => 16#16#,
      Hat3y => 16#17#,
      Pressure => 16#18#,
      Distance => 16#19#,
      Tilt_X => 16#1a#,
      Tilt_Y => 16#1b#,
      TOol_Width => 16#1c#,
      Volume => 16#20#,
      Misc => 16#28#,
      MT_Slot => 16#2f#,
      MT_Touch_Major => 16#30#,
      MT_Touch_Minor => 16#31#,
      MT_Width_Major => 16#32#,
      MT_Width_Minor => 16#33#,
      MT_Orientation => 16#34#,
      MT_Position_X => 16#35#,
      MT_Position_Y => 16#36#,
      MT_Tool_Type => 16#37#,
      MT_Blob_Id => 16#38#,
      MT_Tracking_Id => 16#39#,
      MT_Pressure => 16#3a#,
      MT_Distance => 16#3b#,
      MT_Tool_X => 16#3c#,
      MT_Tool_Y => 16#3d#);

   type Misc_Code is
     (Serial,
      Pulseled,
      Gesture,
      Raw,
      Scan,
      Timestamp);
   for Misc_Code'Size use Event_Code'Size;
   for Misc_Code use
     (Serial => 16#00#,
      Pulseled => 16#01#,
      Gesture => 16#02#,
      Raw => 16#03#,
      Scan => 16#04#,
      Timestamp => 16#05#);

   type Switch_Code is
     (Lid,
      Tablet_Mode,
      Headphone_Insert,
      Rfkill_All,
      Microphone_Insert,
      Dock,
      Lineout_Insert,
      Jack_Physical_Insert,
      Videoout_Insert,
      Camera_Lens_Cover,
      Keypad_Slide,
      Front_Proximity,
      Rotate_Lock,
      Linein_Insert,
      Mute_Device);
   for Switch_Code'Size use Event_Code'Size;
   for Switch_Code use
     (Lid => 16#00#,
      Tablet_Mode => 16#01#,
      Headphone_Insert => 16#02#,
      Rfkill_All => 16#03#,
      Microphone_Insert => 16#04#,
      Dock => 16#05#,
      Lineout_Insert => 16#06#,
      Jack_Physical_Insert => 16#07#,
      Videoout_Insert => 16#08#,
      Camera_Lens_Cover => 16#09#,
      Keypad_Slide => 16#0a#,
      Front_Proximity => 16#0b#,
      Rotate_Lock => 16#0c#,
      Linein_Insert => 16#0d#,
      Mute_Device => 16#0e#);

   Radio : constant Switch_Code := Rfkill_All;

   type Led_Code is
     (Numl,
      Capsl,
      Scrolll,
      Compose,
      Kana,
      Sleep,
      Suspend,
      Mute,
      Misc,
      Mail,
      Charging);
   for Led_Code'Size use Event_Code'Size;
   for Led_Code use
     (Numl => 16#00#,
      Capsl => 16#01#,
      Scrolll => 16#02#,
      Compose => 16#03#,
      Kana => 16#04#,
      Sleep => 16#05#,
      Suspend => 16#06#,
      Mute => 16#07#,
      Misc => 16#08#,
      Mail => 16#09#,
      Charging => 16#0a#);

   type Sound_Code is
     (Click,
      Bell,
      Tone);
   for Sound_Code'Size use Event_Code'Size;
   for Sound_Code use
     (Click => 16#00#,
      Bell => 16#01#,
      Tone => 16#02#);

   type Autorepeat_Code is
     (Repeat_Delay,
      Period);
   for Autorepeat_Code'Size use Event_Code'Size;
   for Autorepeat_Code use
     (Repeat_Delay => 16#00#,
      Period => 16#01#);

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Synchronization_Code, Event_Code);

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Key_And_Button_Code, Event_Code);

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Relative_Axis_Code, Event_Code);

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Absolute_Axis_Code, Event_Code); -- contains Multi_Touch_Code

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Misc_Code, Event_Code);

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Switch_Code, Event_Code);

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Led_Code, Event_Code);

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Sound_Code, Event_Code);

   function To_Event_Code is new Ada.Unchecked_Conversion
     (Autorepeat_Code, Event_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Synchronization_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Key_And_Button_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Relative_Axis_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Absolute_Axis_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Misc_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Switch_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Led_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Sound_Code);

   function From_Event_Code is new Ada.Unchecked_Conversion
     (Event_Code, Autorepeat_Code);

   type Event_Kind  is record
      Event_Type : Libevdev.Types.Event_Type;
      Event_Code : Libevdev.Types.Event_Code;
   end record;

   function To_Event_Kind (Code : Synchronization_Code) return Event_Kind;
   function To_Event_Kind (Code : Key_And_Button_Code) return Event_Kind;
   function To_Event_Kind (Code : Relative_Axis_Code) return Event_Kind;
   function To_Event_Kind (Code : Absolute_Axis_Code) return Event_Kind;
   function To_Event_Kind (Code : Misc_Code) return Event_Kind;
   function To_Event_Kind (Code : Switch_Code) return Event_Kind;
   function To_Event_Kind (Code : Led_Code) return Event_Kind;
   function To_Event_Kind (Code : Sound_Code) return Event_Kind;
   function To_Event_Kind (Code : Autorepeat_Code) return Event_Kind;

   -- aliases and stuff for Multi_Touch_Stuff
   subtype Multi_Touch_Code is Absolute_Axis_Code range MT_Slot .. MT_Tool_Y;
   Multi_Touch : constant Event_Type := Absolute_Axis;

   function Is_Multi_Touch (Kind : Event_Kind) return Boolean;

   Event_Kind_First : constant Event_Kind :=
     (Event_Type => Event_Type'First,
      Event_Code => To_Event_Code(Synchronization_Code'First));

   Event_Kind_Last : constant Event_Kind :=
     (Event_Type => Event_Type'Last,
      Event_Code => To_Event_Code(Autorepeat_Code'Last));

   Event_Kind_Count : constant Positive :=
     Synchronization_Code'Pos (Synchronization_Code'Last) -
     Synchronization_Code'Pos (Synchronization_Code'First) + 1 +

     Key_And_Button_Code'Pos (Key_And_Button_Code'Last) -
     Key_And_Button_Code'Pos (Key_And_Button_Code'First) + 1 +

     Relative_Axis_Code'Pos (Relative_Axis_Code'Last) -
     Relative_Axis_Code'Pos (Relative_Axis_Code'First) + 1 +

     Absolute_Axis_Code'Pos (Absolute_Axis_Code'Last) -
     Absolute_Axis_Code'Pos (Absolute_Axis_Code'First) + 1 +

     Misc_Code'Pos (Misc_Code'Last) -
     Misc_Code'Pos (Misc_Code'First) + 1 +

     Switch_Code'Pos (Switch_Code'Last) -
     Switch_Code'Pos (Switch_Code'First) + 1 +

     Led_Code'Pos (Led_Code'Last) -
     Led_Code'Pos (Led_Code'First) + 1 +

     Autorepeat_Code'Pos (Autorepeat_Code'Last) -
     Autorepeat_Code'Pos (Autorepeat_Code'First) + 1;

   function Event_Kind_Succ (Current : Event_Kind) return Event_Kind with
     Pre => Current /= Event_Kind_Last;

   function Event_Kind_Pred (Current : Event_Kind) return Event_Kind with
     Pre => Current /= Event_Kind_First;

   procedure For_Each_Event_Kind
     (Action : not null access procedure (Kind : Event_Kind));

   function Image (Event : Event_Kind) return String;

   -- from linux/time.h
   type Timeval is record
      Seconds : aliased time_t;  -- /usr/include/bits/time.h:32
      Micro_Seconds : aliased suseconds_t;  -- /usr/include/bits/time.h:33
   end record with
     Convention => C_Pass_By_Copy;

   -- from linux/input.h
   type Input_Event is record
      Time : aliased Timeval;  -- /usr/include/linux/input.h:23
      Ev_Type : aliased Event_Type;  -- /usr/include/linux/input.h:24
      Code : aliased Event_Code;  -- /usr/include/linux/input.h:25
      Value : aliased Signed_32;  -- /usr/include/linux/input.h:26
   end record with
     Convention => C_Pass_By_Copy;  -- /usr/include/linux/input.h:22

   -- from linux/input.h
   type Input_Absinfo is record
      Value : Signed_32;  -- /usr/include/linux/input.h:66
      Minimum : Signed_32;  -- /usr/include/linux/input.h:67
      Maximum : Signed_32;  -- /usr/include/linux/input.h:68
      Fuzz : Signed_32;  -- /usr/include/linux/input.h:69
      Flat : Signed_32;  -- /usr/include/linux/input.h:70
      Resolution : Signed_32;  -- /usr/include/linux/input.h:71
   end record with
     Convention => C_Pass_By_Copy;

   subtype Absolute_Axis_Info is Input_Absinfo;

   type Event is record
      Time : Ada.Real_Time.Time;
      Kind : Event_Kind;
      Value : Signed_32;
   end record;

   function Input_Event_To_Event (Input : Input_Event) return Event;

   -- TODO: does the convention do the right thing?
   type Read_Flags is record
      Sync : Boolean;
      Normal : Boolean;
      Force_Sync : Boolean;
      Blocking : Boolean;
   end record with
     Convention => C_Pass_By_Copy;
   for Read_Flags'Size use Read_Flags_Underlying'Size;
   for Read_Flags use record
      Sync at 0 range 0 .. 0;
      Normal at 1 range 1 .. 1;
      Force_Sync at 2 range 2 .. 2;
      Blocking at 3 range 3 .. 3;
   end record;

   -- in any other event we throw an exception
   type Read_Status is (Success, Sync, No_New_Events);

   --  type Event_Kinds is array (Positive range <>) of Event_Kind;
   --  type Absolute_Infos is array (Positive range <>) of Absolute_Axis_Info;
   --  type Multi_Touch_Infos is array (Positive range <>) of Absolute_Axis_Info;

   type Clock_Type is new int;
   Monotonic : constant Clock_Type with
     Import, Convention => C, Link_Name => "clock_monotonic";

   Real_Time : constant Clock_Type with
     Import, Convention => C, Link_Name => "clock_realtime";

private
end Libevdev.Types;
