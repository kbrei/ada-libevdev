with Ada.Exceptions; use Ada.Exceptions; 
with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings; use Interfaces.C.Strings;

with Linux.Errno; use Linux.Errno;

package body Linux.File
is
   function Open
     (Path : String;
      Options : Linux.Open.Flags := Linux.Open.Read_Write;
      Create_Rights : Linux.Rights.Flags := Linux.Rights.No_Rights)
     return Descriptor
   is
      -- TODO: should have mode_t !
      function Raw_Open
        (Path : chars_ptr;
         Flags : Linux.Open.Flags;
         Create_Rights : Linux.Rights.Flags)
        return int with
          Import, Convention => C, Link_Name => "open";

      C_Path : chars_ptr := New_String (Path); 

      Open_Result : int;
   begin
      Open_Result := Raw_Open (C_Path, Options, Create_Rights);

      Free (C_Path);

      Check_Errno
        (Return_Value => Open_Result,
         Message_Prefix => "Error while opening file '" & Path & "' : ");

      -- TODO: is this cast sound?
      -- What about Zero?
      return Descriptor (Open_Result);
   end;

   procedure Close (File_Descriptor : Descriptor)
   is
      function Raw_Close (File_Descriptor : Descriptor) return int with
        Import, Convention => C, Link_Name => "close";

      Close_Result : int;
   begin
      Close_Result := Raw_Close (File_Descriptor);

      Check_Errno (Close_Result);

      -- TODO: what if Close_Result /= 0 and Colse_Result /= -1
   end;

end Linux.File;
