package body Libevdev.Types is
   function Event_Kind_Succ (Current : Event_Kind) return Event_Kind is
      generic
         type Current_Code is (<>);
         Current_Type : Event_Type;

         type Next_Code is (<>);
         Next_Type : Event_Type;

         with function To_Event_Code
           (Value : Current_Code)
           return Event_Code is <>;

         with function From_Event_Code
           (Value : Event_Code)
           return Current_Code is <>;

         with function To_Event_Code
           (Value : Next_Code)
           return Event_Code is <>;

      function Next (Kind : Event_Kind) return Event_Kind with Inline;

      function Next (Kind : Event_Kind) return Event_Kind is

         function Current_Succ (Code : Event_Code) return Event_Code is
            Curr_Code : constant Current_Code := From_Event_Code (Code);
            Next_Code : constant Current_Code := Current_Code'Succ (Curr_Code);
         begin
            return To_Event_Code (Next_Code);
         end;

         pragma Inline (Current_Succ);

      begin
         if From_Event_Code (Kind.Event_Code) = Current_Code'Last then
            return
              (Event_Type => Next_Type,
               Event_Code => To_Event_Code (Next_Code'First));
         else
            return
              (Event_Type => Current_Type,
               Event_Code => Current_Succ (Kind.Event_Code));
         end if;
      end;

      function Next_Sync is new Next
        (Synchronization_Code, Synchronization,
         Key_And_Button_Code, Key_And_Button);

      function Next_Key is new Next
        (Key_And_Button_Code, Key_And_Button,
         Relative_Axis_Code, Relative_Axis);

      function Next_Rel is new Next
        (Relative_Axis_Code, Relative_Axis,
         Absolute_Axis_Code, Absolute_Axis);

      function Next_Abs is new Next
        (Absolute_Axis_Code, Absolute_Axis,
         Misc_Code, Misc);

      function Next_Misc is new Next
        (Misc_Code, Misc,
         Switch_Code, Switch);

      function Next_Switch is new Next
        (Switch_Code, Switch,
         Led_Code, Led);

      function Next_Led is new Next
        (Switch_Code, Switch,
         Sound_Code, Sound);

      function Next_Sound is new Next
        (Sound_Code, Sound,
         Autorepeat_Code, Autorepeat);

   begin
      case (Current.Event_Type) is
         when Synchronization => return Next_Sync (Current);
         when Key_And_Button => return Next_Key (Current);
         when Relative_Axis => return Next_Rel (Current);
         when Absolute_Axis => return Next_Abs (Current);
         when Misc => return Next_Misc (Current);
         when Switch => return Next_Switch (Current);
         when Led => return Next_Led (Current);
         when Sound => return Next_Sound (Current);
         when Autorepeat =>
            declare
               Curr_Auto : constant  Autorepeat_Code :=
                 From_Event_Code (Current.Event_Code);

               Next_Event_Code : constant  Event_Code :=
                 To_Event_Code (Autorepeat_Code'Succ (Curr_Auto));
            begin
               return (Event_Type => Autorepeat, Event_Code => Next_Event_Code);
            end;
      end case;
   end Event_Kind_Succ;

   function Event_Kind_Pred (Current : Event_Kind) return Event_Kind is
      generic
         type Current_Code is (<>);
         Current_Type : Event_Type;

         type Previous_Code is (<>);
         Previous_Type : Event_Type;

         with function To_Event_Code
           (Value : Previous_Code)
           return Event_Code is <>;

         with function To_Event_Code
           (Value : Current_Code)
           return Event_Code is <>;

         with function From_Event_Code
           (Value : Event_Code)
           return Current_Code is <>;

      function Previous (Kind : Event_Kind) return Event_Kind with Inline;

      function Previous (Kind : Event_Kind) return Event_Kind is
         function Current_Pred (Code : Event_Code) return Event_Code is
            Curr_Code : constant Current_Code := From_Event_Code (Code);
            Prev_Code : constant Current_Code := Current_Code'Pred (Curr_Code);
         begin
            return To_Event_Code (Prev_Code);
         end;

         pragma Inline (Current_Pred);

      begin
            if From_Event_Code (Kind.Event_Code) = Current_Code'First then
               return
                 (Event_Type => Previous_Type,
                  Event_Code => To_Event_Code (Previous_Code'Last));
            else
               return
                 (Event_Type => Current_Type,
                  Event_Code => Current_Pred (Kind.Event_Code));
            end if;
      end;

      function Previous_Key is new Previous
        (Key_And_Button_Code, Key_And_Button,
         Synchronization_Code, Synchronization);

      function Previous_Rel is new Previous
        (Relative_Axis_Code, Relative_Axis,
         Key_And_Button_Code, Key_And_Button);

      function Previous_Abs is new Previous
        (Absolute_Axis_Code, Absolute_Axis,
         Relative_Axis_Code, Relative_Axis);

      function Previous_Misc is new Previous
        (Misc_Code, Misc,
         Absolute_Axis_Code, Absolute_Axis);

      function Previous_Switch is new Previous
        (Switch_Code, Switch,
         Misc_Code, Misc);

      function Previous_Led is new Previous
        (Led_Code, Led,
         Switch_Code, Switch);

      function Previous_Sound is new Previous
        (Sound_Code, Sound,
         Led_Code, Led);

      function Previous_Autorepeat is new Previous
        (Autorepeat_Code, Autorepeat,
         Sound_Code, Sound);
   begin
      case (Current.Event_Type) is
         when Synchronization =>
            declare
               Curr_Sync : constant Synchronization_Code :=
                 From_Event_Code (Current.Event_Code);

               Prev_Event_Code : constant  Event_Code :=
                 To_Event_Code (Synchronization_Code'Succ (Curr_Sync));
            begin
               return (Event_Type => Autorepeat, Event_Code => Prev_Event_Code);
            end;
         when Key_And_Button => return Previous_Key (Current);
         when Relative_Axis => return Previous_Rel (Current);
         when Absolute_Axis => return Previous_Abs (Current);
         when Misc => return Previous_Misc (Current);
         when Switch => return Previous_Switch (Current);
         when Led => return Previous_Led (Current);
         when Sound => return Previous_Sound (Current);
         when Autorepeat => return Previous_Autorepeat (Current);
      end case;
   end Event_Kind_Pred;

   procedure For_Each_Event_Kind
     (Action : not null access procedure (Kind : Event_Kind))
   is
      Current : Event_Kind := Event_Kind_First;
   begin
      while (Current /= Event_Kind_Last) loop
         Action (Current);
         Current := Event_Kind_Succ (Current);
      end loop;
   end;

   function Image (Event : Event_Kind) return String
   is
   begin
      case (Event.Event_Type) is
         when Synchronization => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Synchronization_Code'Image (From_Event_Code (Event.Event_Code));
         when Key_And_Button => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Key_And_Button_Code'Image (From_Event_Code (Event.Event_Code));
         when Relative_Axis => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Relative_Axis_Code'Image (From_Event_Code (Event.Event_Code));
         when Absolute_Axis => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Absolute_Axis_Code'Image (From_Event_Code (Event.Event_Code));
         when Misc => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Misc_Code'Image (From_Event_Code (Event.Event_Code));
         when Switch => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Switch_Code'Image (From_Event_Code (Event.Event_Code));
         when Led => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Led_Code'Image (From_Event_Code (Event.Event_Code));
         when Sound => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Sound_Code'Image (From_Event_Code (Event.Event_Code));
         when Autorepeat => return
           Event_Type'Image (Event.Event_Type) & ": " &
           Autorepeat_Code'Image (From_Event_Code (Event.Event_Code));
      end case;
   end Image;


   function To_Event_Kind (Code : Synchronization_Code) return Event_Kind is
     ((Synchronization, To_Event_Code (Code)));
   function To_Event_Kind (Code : Key_And_Button_Code) return Event_Kind is
     ((Key_And_Button, To_Event_Code (Code)));
   function To_Event_Kind (Code : Relative_Axis_Code) return Event_Kind is
     ((Relative_Axis, To_Event_Code (Code)));
   function To_Event_Kind (Code : Absolute_Axis_Code) return Event_Kind is
     ((Absolute_Axis, To_Event_Code (Code)));
   function To_Event_Kind (Code : Misc_Code) return Event_Kind is
     ((Misc, To_Event_Code (Code)));
   function To_Event_Kind (Code : Switch_Code) return Event_Kind is
     ((Switch, To_Event_Code (Code)));
   function To_Event_Kind (Code : Led_Code) return Event_Kind is
     ((Led, To_Event_Code (Code)));
   function To_Event_Kind (Code : Sound_Code) return Event_Kind is
     ((Sound, To_Event_Code (Code)));
   function To_Event_Kind (Code : Autorepeat_Code) return Event_Kind is
     ((Autorepeat, To_Event_Code (Code)));

   function Is_Multi_Touch (Kind : Event_Kind) return Boolean is
     ((Kind.Event_Type = Multi_Touch and then
         From_Event_Code (Kind.Event_Code) in Multi_Touch_Code'Range));

   function Input_Event_To_Event (Input : Input_Event) return Event is

      function Timeval_To_Time (Value : Timeval) return Time is
         Seconds : constant Seconds_Count :=
           Seconds_Count (Value.Seconds);
         MSeconds : constant Time_Span :=
           Microseconds (Integer (Value.Micro_Seconds));
      begin
         return Time_Of (Seconds, MSeconds);
      end;

      Time : constant Ada.Real_Time.Time := Timeval_To_Time (Input.Time);

      Kind : constant Event_Kind :=
        (Event_Type => Input.Ev_type,
         Event_Code => Input.Code);
   begin
      return (Time, Kind, Input.Value);
   end;


end Libevdev.Types;
