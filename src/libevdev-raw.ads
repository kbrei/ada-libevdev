with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with System;

with Linux.File;
with Libevdev.Types; use Libevdev.Types;

-- This package binds everything as good as it can without wrapping
-- the functions
-- So we use more fancy input types, but do not do anything with the return types.
-- Wrapping to a nicer interface is done in Libevdev.Nice
package Libevdev.Raw is
   package Types renames Libevdev.Types;

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:765
   function New_Evdev return Evdev;
   pragma Import (C, New_Evdev, "libevdev_new");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:789
   function New_From_FD
     (File_Descriptor : Linux.File.Descriptor;
      Dev : in out Evdev)
     return int;
   pragma Import (C, new_from_fd, "libevdev_new_from_fd");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:801
   procedure Free (Dev : Evdev);
   pragma Import (C, free, "libevdev_free");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:930
   procedure Set_Device_Log_Function
     (Dev : Evdev;
      Logfunc : Device_Log_Func;
      Priority : Log_Priority;
      Data : System.Address);
   pragma Import (C, Set_Device_Log_Function, "libevdev_set_device_log_function");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:961
   function Grab (Dev : Evdev; Mode : Grab_Mode) return int;
   pragma Import (C, grab, "libevdev_grab");

   -- TODO: we need to bind open and errno as well!
   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:986
   function Set_FD (Dev : Evdev; File_Descriptor : Linux.File.Descriptor) return int;
   pragma Import (C, Set_FD, "libevdev_set_fd");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1023
   function Change_FD (Dev : Evdev; File_Descriptor : Linux.File.Descriptor) return int;
   pragma Import (C, Change_FD, "libevdev_change_fd");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1033
   function Get_FD (Dev : Evdev) return int;
   pragma Import (C, Get_FD, "libevdev_get_fd");

   -- TODO: nice read status
   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1038
   --  type Read_Status is (Success, Status_Sync);

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1107
   function Next_Event
     (Dev : Evdev;
      Flags : Read_Flags; -- should be Read_Flags, we should create a curtom flag type
      Event : out Input_Event) return int;
   pragma Import (C, Next_Event, "libevdev_next_event");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1131
   function Has_Event_Pending (Dev : Evdev) return int;
   pragma Import (C, Has_Event_Pending, "libevdev_has_event_pending");

   function Get_Name (Dev : Evdev) return chars_ptr;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1147
   pragma Import (C, Get_Name, "libevdev_get_name");

   procedure Set_Name (Dev : Evdev; Name : chars_ptr);  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1162
   pragma Import (C, Set_Name, "libevdev_set_name");

   function Get_Phys (Dev : Evdev) return chars_ptr;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1179
   pragma Import (C, Get_Phys, "libevdev_get_phys");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1194
   procedure Set_Phys (Dev : Evdev; Name : chars_ptr);
   pragma Import (C, set_phys, "libevdev_set_phys");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1209
   function Get_Uniq (Dev : Evdev) return chars_ptr;
   pragma Import (C, Get_Uniq, "libevdev_get_uniq");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1224
   procedure Set_Uniq (Dev : Evdev; Uniq : chars_ptr);
   pragma Import (C, Set_Uniq, "libevdev_set_uniq");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1235
   function Get_Id_Product (Dev : Evdev) return int;
   pragma Import (C, Get_Id_Product, "libevdev_get_id_product");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1246
   procedure Set_Id_Product (Dev : Evdev; Product_Id : int);
   pragma Import (C, Set_Id_Product, "libevdev_set_id_product");

   function Get_Id_Vendor (Dev : Evdev) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1257
   pragma Import (C, Get_Id_Vendor, "libevdev_get_id_vendor");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1268
   procedure Set_Id_Vendor (Dev : Evdev; Vendor_Id : int);
   pragma Import (C, Set_Id_Vendor, "libevdev_set_id_vendor");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1279
   function Get_Id_Bustype (Dev : Evdev) return int;
   pragma Import (C, Get_Id_Bustype, "libevdev_get_id_bustype");

   -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1290
   procedure Set_Id_Bustype (Dev : Evdev; Bustype : int);
   pragma Import (C, Set_Id_Bustype, "libevdev_set_id_bustype");

   function Get_Id_Version (Dev : Evdev) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1301
   pragma Import (C, get_id_version, "libevdev_get_id_version");

   procedure Set_Id_Version (Dev : Evdev; Param : int);  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1312
   pragma Import (C, set_id_version, "libevdev_set_id_version");

   function Get_Driver_Version (Dev : Evdev) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1323
   pragma Import (C, Get_Driver_Version, "libevdev_get_driver_version");

   function Has_Property (Dev : Evdev; Prop : Property) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1335
   pragma Import (C, Has_Property, "libevdev_has_property");

   function Enable_Property (Dev : Evdev; Prop : Property) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1348
   pragma Import (C, Enable_Property, "libevdev_enable_property");

   function Has_Event_Type (Dev : Evdev; Event_Type : Types.Event_Type) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1360
   pragma Import (C, has_event_type, "libevdev_has_event_type");

   function Has_Event_Code
     (Dev : Evdev;
      Event_Type : Types.Event_Type;
      Code : Event_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1373
   pragma Import (C, Has_Event_Code, "libevdev_has_event_code");

   function Get_Abs_Minimum (Dev : Evdev; Code : Absolute_Axis_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1387
   pragma Import (C, Get_Abs_Minimum, "libevdev_get_abs_minimum");

   function Get_Abs_Maximum (Dev : Evdev; Code : Absolute_Axis_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1401
   pragma Import (C, Get_Abs_Maximum, "libevdev_get_abs_maximum");

   function Get_Abs_Fuzz (Dev : Evdev; Code : Absolute_Axis_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1415
   pragma Import (C, Get_Abs_Fuzz, "libevdev_get_abs_fuzz");

   function Get_Abs_Flat (Dev : Evdev; Code : Absolute_Axis_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1429
   pragma Import (C, Get_Abs_Flat, "libevdev_get_abs_flat");

   function Get_Abs_Resolution (Dev : Evdev; Code : Absolute_Axis_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1443
   pragma Import (C, Get_Abs_Resolution, "libevdev_get_abs_resolution");

   function Get_Abs_Info (Dev : Evdev; Code : Absolute_Axis_Code) return access Input_Absinfo;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1458
   pragma Import (C, Get_Abs_Info, "libevdev_get_abs_info");

   function Get_Event_Value
     (Dev : Evdev;
      Event_Type : Types.Event_Type;
      Code : Event_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1482
   pragma Import (C, Get_Event_Value, "libevdev_get_event_value");

   function Set_Event_Value
     (Dev : Evdev;
      Event_Type : Types.Event_Type;
      Code : Event_Code;
      Value : int) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1515
   pragma Import (C, Set_Event_Value, "libevdev_set_event_value");

   -- TODO: Should be out parameter
   function Fetch_Event_Value
     (Dev : Evdev;
      Event_Type : Types.Event_Type;
      Code : Event_Code;
      Value : in out int) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1542
   pragma Import (C, Fetch_Event_Value, "libevdev_fetch_event_value");

   function Get_Slot_Value
     (Dev : Evdev;
      Slot : unsigned;
      Code : Event_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1564
   pragma Import (C, Get_Slot_Value, "libevdev_get_slot_value");

   function Set_Slot_Value
     (Dev : Evdev;
      Slot : unsigned;
      Code : Event_Code;
      Value : int) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1592
   pragma Import (C, set_slot_value, "libevdev_set_slot_value");

   -- TODO: Should be out parameter
   function Fetch_Slot_Value
     (Dev : Evdev;
      Slot : unsigned;
      Code : Event_Code;
      Value : in out int) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1619
   pragma Import (C, Fetch_Slot_Value, "libevdev_fetch_slot_value");

   function Get_Num_Slots (Dev : Evdev) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1634
   pragma Import (C, Get_Num_Slots, "libevdev_get_num_slots");

   function Get_Current_Slot (Dev : Evdev) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1651
   pragma Import (C, Get_Current_Slot, "libevdev_get_current_slot");

   procedure Set_Abs_Minimum
     (Dev : Evdev;
      Code : Absolute_Axis_Code;
      Min : int);  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1664
   pragma Import (C, Set_Abs_Minimum, "libevdev_set_abs_minimum");

   procedure Set_Abs_Maximum
     (Dev : Evdev;
      Code : Absolute_Axis_Code;
      Max : int);  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1677
   pragma Import (C, Set_Abs_Maximum, "libevdev_set_abs_maximum");

   procedure Set_Abs_Fuzz
     (Dev : Evdev;
      Code : Absolute_Axis_Code;
      Fuzz : int);  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1690
   pragma Import (C, Set_Abs_Fuzz, "libevdev_set_abs_fuzz");

   procedure Set_Abs_Flat
     (Dev : Evdev;
      Code : Absolute_Axis_Code;
      Flat : int);  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1703
   pragma Import (C, Set_Abs_Flat, "libevdev_set_abs_flat");

   procedure Set_Abs_Resolution
     (Dev : Evdev;
      Code : Absolute_Axis_Code;
      Resolution : int);  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1716
   pragma Import (C, Set_Abs_Resolution, "libevdev_set_abs_resolution");

   procedure Set_Abs_Info
     (Dev : Evdev;
      Code : Absolute_Axis_Code;
      Absolute : in out Input_Absinfo);  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1729
   pragma Import (C, Set_Abs_Info, "libevdev_set_abs_info");

   function Enable_Event_Type (Dev : Evdev; Event_Type : Types.Event_Type) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1748
   pragma Import (C, Enable_Event_Type, "libevdev_enable_event_type");

   function Disable_Event_Type (Dev : Evdev; Event_Type : Types.Event_Type) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1776
   pragma Import (C, Disable_Event_Type, "libevdev_disable_event_type");

   -- TODO: What should we do about this stupid pseudo union (Data)
   function Enable_Event_Code
     (Dev : Evdev;
      Event_Type : Types.Event_Type;
      Code : Event_Code;
      Data : System.Address) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1811
   pragma Import (C, Enable_Event_Code, "libevdev_enable_event_code");

   function Disable_Event_Code
     (Dev : Evdev;
      Event_Type : Types.Event_Type;
      Code : Event_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1840
   pragma Import (C, disable_event_code, "libevdev_disable_event_code");

   function Kernel_Set_Abs_Info
     (Dev : Evdev;
      Code : Absolute_Axis_Code;
      Absinfo : in out Input_Absinfo) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1856
   pragma Import (C, kernel_set_abs_info, "libevdev_kernel_set_abs_info");

   function Kernel_Set_Led_Value
     (Dev : Evdev;
      Code : Absolute_Axis_Code;
      Value : Led_Value) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1879
   pragma Import (C, Kernel_Set_Led_Value, "libevdev_kernel_set_led_value");

   --  TODO: How do we bind the va_list?
   function Kernel_Set_led_Values (Dev : Evdev) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1904
   pragma Import (C, kernel_set_led_values, "libevdev_kernel_set_led_values");

   function Set_Clock_Id (Dev : Evdev; Clock_Id : int) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1920
   pragma Import (C, Set_Clock_Id, "libevdev_set_clock_id");

   function Event_Is_Type (Ev :in out Input_Event; Event_Type : Types.Event_Type) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1943
   pragma Import (C, Event_Is_Type, "libevdev_event_is_type");

   function Event_Is_Code
     (Ev : in out Input_Event;
      Event_Type : Types.Event_Type;
      Code : Event_Code) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1968
   pragma Import (C, Event_Is_Code, "libevdev_event_is_code");

   function Event_Type_Get_Name (Event_Type : Types.Event_Type) return chars_ptr;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1981
   pragma Import (C, event_type_get_name, "libevdev_event_type_get_name");

   function Event_Code_Get_Name (Event_Type : Types.Event_Type; Code : Event_Code) return chars_ptr;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:1994
   pragma Import (C, Event_Code_Get_Name, "libevdev_event_code_get_name");

   function Property_Get_Name (Prop : Property) return chars_ptr;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2009
   pragma Import (C, Property_Get_Name, "libevdev_property_get_name");

   function Event_Type_Get_Max (Event_Type : Types.Event_Type) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2023
   pragma Import (C, Event_Type_Get_Max, "libevdev_event_type_get_max");

   function Event_Type_From_Name (Name : chars_ptr) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2039
   pragma Import (C, Event_Type_From_Name, "libevdev_event_type_from_name");

   function Event_Type_From_Name_n (Name : chars_ptr; Length : size_t) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2057
   pragma Import (C, Event_Type_From_Name_n, "libevdev_event_type_from_name_n");

   function Event_Code_From_Name (Event_Type : Types.Event_Type; Name : chars_ptr) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2078
   pragma Import (C, Event_Code_From_Name, "libevdev_event_code_from_name");

   function Event_Code_From_Name_n
     (Event_Type : Types.Event_Type;
      Name : chars_ptr;
      Length : size_t) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2101
   pragma Import (C, event_code_from_name_n, "libevdev_event_code_from_name_n");

   function Property_From_Name (Name : chars_ptr) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2116
   pragma Import (C, Property_From_Name, "libevdev_property_from_name");

   function Property_From_Name_n (Name : chars_ptr; Length : size_t) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2132
   pragma Import (C, Property_From_Name_n, "libevdev_property_from_name_n");

   function Get_Repeat
     (Dev : Evdev;
      Delay_Duration : in out int;
      Period : in out int) return int;  -- /usr/include/libevdev-1.0/libevdev/libevdev.h:2151
   pragma Import (C, Get_Repeat, "libevdev_get_repeat");
end Libevdev.Raw;
