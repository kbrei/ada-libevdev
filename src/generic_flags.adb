package body Generic_Flags
is
   function To_Underlying (From : Flags_Type) return Underlying
     is (Underlying (From));
   pragma Inline (To_Underlying);

   function Add (To : Flags_Type; To_Add : Flags_Type) return Flags_Type
     is (To or To_Add);
   pragma Inline (Add);

   function Remove (From : Flags_Type; To_Remove : Flags_Type) return Flags_Type
     is (From and (not To_Remove));
   pragma Inline (Remove);

   function Toggle (Inside : Flags_Type; To_Toggle : Flags_Type) return Flags_Type
     is (Inside xor To_Toggle);
   pragma Inline (Toggle);

   function Contains (Set : Flags_Type; Subset : Flags_Type) return Boolean
     is (Equal ((Set and Subset), Subset));
   pragma Inline (Contains);
   
   function Equal (Left : Flags_Type; Right : Flags_Type) return Boolean
     is (To_Underlying (Left) = To_Underlying (Right));
   pragma Inline (Equal);

   package body Unsafe
   is

      function From_Underlying (Base : Underlying) return Flags_Type
        is (Flags_Type (Base));
      pragma Inline (From_Underlying);

   end Unsafe;
end Generic_Flags;
