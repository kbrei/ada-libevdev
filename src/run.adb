with Ada.Containers.Vectors;
with Ada.Text_IO;
with Ada.Strings.Fixed;
with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings; use Interfaces.C.Strings;
with Ada.Unchecked_Conversion;

with Linux.Open;
with Linux.File;
with Linux.Rights;
with Linux.Errno;
use Linux.Open.Operators;
use Linux;

with Libevdev.Nice;
with Libevdev.Types; use Libevdev.Types;
with System;
with System.Address_Image;

function Run return Integer
is
   package IO renames Ada.Text_IO;
begin
   IO.Put_Line (Natural'Image (Natural'First));

   return 1;
end Run;
