with Ada.Finalization; use Ada.Finalization;

with Libevdev.Raw;
with Linux.Open;

package body Libevdev.Nice is
   package Raw renames Libevdev.Raw;

   ---------------
   -- New_Evdev --
   ---------------

   function New_Evdev
     (File_Descriptor : Linux.File.Descriptor)
      return Evdev
   is
      Raw_Evdev : Types.Evdev;
      Set_FD_Result : int;
   begin
      Set_FD_Result := Raw.New_From_FD
        (File_Descriptor => File_Descriptor,
         Dev => Raw_Evdev);

      Check_Negative_Errno
        (Return_Value => Set_FD_Result,
         Message_Prefix => "While setting file descriptor");

      return (Controlled with Raw_Device => Raw_Evdev);
   end New_Evdev;

   ---------------
   -- New_Evdev --
   ---------------

   function New_Evdev
     (File_Path : String;
      Blocking : Boolean := False)
     return Evdev
   is
      use Linux.Open.Operators;
      use Linux;

      Blocking_Flag : constant Open.Flags :=
        (if Blocking
         then Open.No_Flags
         else Open.Dont_Block);

      File_Descriptor : constant Linux.File.Descriptor := Linux.File.Open
        (Path => File_Path,
         Options => Open.Read_Only + Blocking_Flag);
   begin
      return New_Evdev (File_Descriptor);
   end New_Evdev;
   
   ---------------------
   -- File_Descriptor --
   ---------------------

   function File_Descriptor (Device : Evdev) return Linux.File.Descriptor
   is
      Result : constant int := Raw.Get_FD (Device.Raw_Device);
   begin
      Check_Negative_Errno
        (Return_Value => Result,
         Evdev_Error_First => True,
         Evdev_Message => "While getting file decriptor");

      return Linux.File.Descriptor (Result);
   end File_Descriptor;

   -------------
   -- Run_Raw --
   -------------
   
   procedure Run_Raw
     (Device : Evdev;
      Action : not null access procedure
        (Raw_Device : Libevdev.Types.Evdev))
   is
   begin
      Action (Device.Raw_Device);
   end;

   ----------
   -- Grab --
   ----------

   procedure Grab (Device : Evdev; Mode : Grab_Mode) is
   begin
      Check_Negative_Errno
        (Return_Value => Raw.Grab(Device.Raw_Device, Mode),
         Message_Prefix => "While grabbing device");
   end Grab;

   ----------------
   -- Next_Event --
   ----------------

   function Next_Event
     (Device : Evdev;
      Flags : Read_Flags;
      Event : out Libevdev.Types.Event)
      return Read_Status
   is
      Input_Ev : Input_Event;
      Next_Event_Result : constant int := Raw.Next_Event (Device.Raw_Device, Flags, Input_Ev);
      Result : Read_Status;
      Event_Defined : Boolean;
   begin
      case Next_Event_Result is
         when 0 => Result := Success;
         when 1 => Result := Sync;
         when others =>
            if Next_Event_Result = - (int (Linux.Errno.EAGAIN)) then
               Result := No_New_Events;
            else
               Check_Negative_Errno
                 (Return_Value => Next_Event_Result,
                  Message_Prefix => "While getting next event");
            end if;
      end case;

      Event_Defined := Result /= No_New_Events;

      if Event_Defined then
         Event := Input_Event_To_Event (Input_Ev);
      end if;

      return Result;
   end Next_Event;

   -----------------------
   -- Has_Event_Pending --
   -----------------------

   function Has_Event_Pending (Device : Evdev) return Boolean is
      Result : constant int := Raw.Has_Event_Pending (Device.Raw_Device);
   begin
      return Check_Boolean 
        (Return_Value => Result,
         Message => "INTERNAL ERROR : Unexpected return value of Raw.Has_Event_Pending!");
   end Has_Event_Pending;

   ----------
   -- Name --
   ----------

   function Name (Device : Evdev) return String is
      -- TODO:  should we free it?
      -- it seems like it's allocated and cached by libevdev
      Raw_Name : constant chars_ptr := Raw.Get_Name (Device.Raw_Device);
      Ada_Name : constant String := Null_Value (Raw_Name);
   begin
      return Ada_Name;
   end Name;

   -----------------------
   -- Physical_Location --
   -----------------------

   function Physical_Location (Device : Evdev) return String is
      -- TODO:  should we free it?
      -- it seems like it's allocated and cached by libevdev
      Raw_Phys : constant chars_ptr := Raw.Get_Phys (Device.Raw_Device);
      Ada_Phys : constant String := Null_Value (Raw_Phys);
   begin
      return Ada_Phys;
   end Physical_Location;

   ------------
   -- Unique --
   ------------

   function Unique (Device : Evdev) return String is
      -- TODO:  should we free it?
      -- it seems like it's allocated and cached by libevdev
      Raw_Unique : constant chars_ptr := Raw.Get_Uniq (Device.Raw_Device);
      Ada_Unique : constant String := Null_Value (Raw_Unique);
   begin
      return Ada_Unique;
   end Unique;

   ----------------
   -- Product_ID --
   ----------------

   function Product_ID (Device : Evdev) return Integer is
      Raw_Product_ID : constant int :=
        Raw.Get_ID_Product(Device.Raw_Device);
   begin
      return Integer (Raw_Product_ID);
   end Product_ID;

   ---------------
   -- Vendor_ID --
   ---------------

   function Vendor_ID (Device : Evdev) return Integer is
      Raw_Vendor_ID : constant int :=
        Raw.Get_ID_Vendor(Device.Raw_Device);
   begin
      return Integer (Raw_Vendor_ID);
   end Vendor_ID;

   ----------------
   -- Bustype_ID --
   ----------------

   function Bustype_ID (Device : Evdev) return Integer is
      Raw_Bustype_ID : constant int :=
        Raw.Get_ID_Bustype(Device.Raw_Device);
   begin
      return Integer (Raw_Bustype_ID);
   end Bustype_ID;

   ----------------
   -- Version_ID --
   ----------------

   function Version_ID (Device : Evdev) return Integer is
      Raw_Version_ID : constant int :=
        Raw.Get_ID_Version(Device.Raw_Device);
   begin
      return Integer (Raw_Version_ID);
   end Version_ID;

   --------------------
   -- Driver_Version --
   --------------------

   function Driver_Version (Device : Evdev) return Integer is
      Raw_Driver_Version_ID : constant int :=
        Raw.Get_Driver_Version(Device.Raw_Device);
   begin
      return Integer (Raw_Driver_Version_ID);
   end Driver_Version;

   ------------------
   -- Has_Property --
   ------------------

   function Has_Property (Device : Evdev; Prop : Property) return Boolean is
      Raw_Has_Prop : constant int :=
        Raw.Has_Property(Device.Raw_Device, Prop);
   begin
      return Check_Boolean
        (Return_Value => Raw_Has_Prop,
         Message => "INTERNAL ERROR: Unexpected return value from Raw.Has_Property");
   end Has_Property;

   ---------------------
   -- Enable_Property --
   ---------------------

   procedure Enable_Property
     (Device : Evdev;
      Prop : Property)
   is
      Enable_Result : constant int :=
        Raw.Enable_Property (Device.Raw_Device, Prop);
   begin
      Check_Negative_Errno
        (Return_Value => Enable_Result,
         Evdev_Error_First => True,
         Evdev_Message => "Error while enabling property: " & Property'Image (Prop));
   end Enable_Property;

   --------------------
   -- Has_Event_Type --
   --------------------

   function Has_Event_Type
     (Device : Evdev;
      Event_Type : Types.Event_Type)
     return Boolean
   is
   begin
      return Check_Boolean
        (Return_Value => Raw.Has_Event_Type (Device.Raw_Device, Event_Type),
         Message => "INTERNAL ERROR: While running Raw.Has_Event_Type");
   end Has_Event_Type;

   ---------------
   -- Has_Event --
   ---------------

   function Has_Event (Device : Evdev; Kind : Event_Kind) return Boolean is
   begin
      return Check_Boolean
        (Return_Value => Raw.Has_Event_Code (Device.Raw_Device, Kind.Event_Type, Kind.Event_Code),
         Message => "INTERNAL ERROR: While running Raw.Has_Event_Code");
   end Has_Event;

   -------------------
   -- Absolute_Info --
   -------------------

   function Absolute_Info
     (Device : Evdev;
      Code : Absolute_Axis_Code)
      return Absolute_Axis_Info
   is
      Info_Ptr : constant access Absolute_Axis_Info := Raw.Get_Abs_Info (Device.Raw_Device, Code);
   begin
      if Info_Ptr = null then
         raise Evdev_Error with
           "Unsupported Eventcode: " & Absolute_Axis_Code'Image (Code);
      end if;

      return Info_Ptr.all;
   end Absolute_Info;

   -----------------------
   -- Set_Absolute_Info --
   -----------------------

   procedure Set_Absolute_Info
     (Device : Evdev;
      Code : Absolute_Axis_Code;
      Value : Absolute_Axis_Info)
   is
      Info : Input_Absinfo := Value;
      Return_Code : constant int := Raw.Kernel_Set_Abs_Info
        (Device.Raw_Device, Code, Info);
   begin
      Check_Negative_Errno 
        (Return_Value => Return_Code,
         Message_Prefix => "Error while setting absolute info");
   end Set_Absolute_Info;

   -----------------------
   -- Change_Absolute_Info --
   -----------------------

   procedure Change_Absolute_Info
     (Device : Evdev;
      Code : Absolute_Axis_Code;
      Change : not null access procedure
        (Info : in out Absolute_Axis_Info))
   is
      Info : Absolute_Axis_Info := Device.Absolute_Info (Code);
   begin
      Change (Info);
      Device.Set_Absolute_Info (Code, Info);
   end;

   -----------
   -- Value --
   -----------

   function Value
     (Device : Evdev;
      Event : Event_Kind)
      return Integer
   is
      Raw_Value : constant int := Raw.Get_Event_Value
        (Dev => Device.Raw_Device,
         Event_Type => Event.Event_Type,
         Code => Event.Event_Code);
   begin
      return Integer (Raw_Value);
   end Value;

   ---------------
   -- Set_Value --
   ---------------

   procedure Set_Value
     (Device : Evdev;
      Event : Event_Kind;
      Value : Integer)
   is
      Result : constant int := Raw.Set_Event_Value
        (Dev => Device.Raw_Device,
         Event_Type => Event.Event_Type,
         Code => Event.Event_Code,
         Value => int (Value));
   begin
      Check_Negative_Errno
        (Return_Value => Result,
         Evdev_Error_First => True,
         Evdev_Message => "Unsopported Event: " & Image (Event));
   end Set_Value;

   ----------------
   -- Slot_Value --
   ----------------

   function Slot_Value
     (Device : Evdev;
      Slot : Natural;
      Code : Multi_Touch_Code)
      return Integer
   is
      Return_Code : constant int := Raw.Get_Slot_Value (Device.Raw_Device, unsigned (Slot), To_Event_Code (Code));
   begin
      -- According to documentiation, a valid value (including 0) is always returned iff the Code is int the ABS_MT_ range
      return Integer (Return_Code);
   end Slot_Value;

   ----------------
   -- Set_Slot_Value --
   ----------------

   procedure Set_Slot_Value
     (Device : Evdev;
      Slot : Natural;
      Code : Multi_Touch_Code;
      Value : Integer)
   is
      Return_Code : constant int := Raw.Set_Slot_Value (Device.Raw_Device, unsigned (Slot), To_Event_Code (Code), int (Value));
   begin
      Check_Negative_Errno
        (Return_Value => Return_Code,
         Evdev_Error_First => True,
         Evdev_Message => "Eventcode: " & Multi_Touch_Code'Image (Code) & " is not enabled.");
   end Set_Slot_Value;

   --------------------------
   -- Supports_Multi_Touch --
   --------------------------

   -- TODO: what exactly is this supposed to do?
   function Supports_Multi_Touch (Device : Evdev) return Boolean is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Supports_Multi_Touch unimplemented");
      raise Program_Error with "Unimplemented function Supports_Multi_Touch";
      return Supports_Multi_Touch (Device);
   end Supports_Multi_Touch;

   ---------------
   -- Num_Slots --
   ---------------

   function Num_Slots
     (Device : Evdev)
      return Natural
   is
      Return_Code : constant int := Raw.Get_Num_Slots (Device.Raw_Device);
   begin
      Check_Negative_Errno
        (Return_Value => Return_Code,
         Evdev_Error_First => True,
         Evdev_Message => "Device does not supports Slots at all.");

      return Natural (Return_Code);
   end Num_Slots;

   ------------------
   -- Current_Slot --
   ------------------

   function Current_Slot
     (Device : Evdev)
      return Natural
   is
      Return_Value : constant int := Raw.Get_Current_Slot (Device.Raw_Device);
   begin

      --  TODO: Messages
      Check_Negative_Errno
        (Return_Value => Return_Value);

      return Natural (Return_Value);
   end Current_Slot;

   -----------------------
   -- Enable_Event_Type --
   -----------------------

   procedure Enable_Event_Type
     (Device : Evdev;
      Event_Type : Types.Event_Type)
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Enable_Event_Type unimplemented");
      raise Program_Error with "Unimplemented procedure Enable_Event_Type";
   end Enable_Event_Type;

   ------------------------
   -- Disable_Event_Type --
   ------------------------

   procedure Disable_Event_Type
     (Device : Evdev;
      Event_Type : Types.Event_Type)
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Disable_Event_Type unimplemented");
      raise Program_Error with "Unimplemented procedure Disable_Event_Type";
   end Disable_Event_Type;

   ------------------
   -- Enable_Event --
   ------------------

   procedure Enable_Event
     (Device : Evdev;
      Event : Event_Kind;
      Config : Event_Config)
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Enable_Event unimplemented");
      raise Program_Error with "Unimplemented procedure Enable_Event";
   end Enable_Event;

   ------------------------
   -- Disable_Event_Code --
   ------------------------

   procedure Disable_Event_Code
     (Device : Evdev;
      Event : Event_Kind)
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Disable_Event_Code unimplemented");
      raise Program_Error with "Unimplemented procedure Disable_Event_Code";
   end Disable_Event_Code;

   -------------------
   -- Set_Led_Value --
   -------------------

   procedure Set_Led_Value
     (Device : Evdev;
      Code : Led_Code;
      Value : Led_Value)
   is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set_Led_Value unimplemented");
      raise Program_Error with "Unimplemented procedure Set_Led_Value";
   end Set_Led_Value;

   --------------------
   -- Set_Led_Values --
   --------------------

   procedure Set_Led_Values (Device : Evdev; Actions : Led_Actions) is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "Set_Led_Values unimplemented");
      raise Program_Error with "Unimplemented procedure Set_Led_Values";
   end Set_Led_Values;

   ----------------------
   -- Set_Clock_Id --
   ----------------------

   procedure Set_Clock_Id (Device : Evdev; Clock_Id : Clock_Type) is
   begin
      --  Generated stub: replace with real body!
      pragma Compile_Time_Warning (Standard.True, "wtionet_Clock_Id unimplemented");
      raise Program_Error with "Unimplemented procedure wtionet_Clock_Id";
   end Set_Clock_Id;

   --------------
   -- Finalize --
   --------------

   procedure Finalize (Device : in out Evdev)
   is
   begin
      Linux.File.Close (Device.File_Descriptor);
      Raw.Free (Device.Raw_Device);
   end Finalize;

   package body Logging is
      procedure Register_Log_Handler
        (Device : in out Evdev;
         Priority : Log_Priority := Error;
         Associated_Data : User_Log_Data_Access)
      is
         Data_Address : constant System.Address :=  Associated_Data.all'Address;
      begin
         Raw.Set_Device_Log_Function
           (Dev => Device.Raw_Device,
            Logfunc => Proxy_Log_Handler_Access,
            Priority => Priority,
            Data => Data_Address);
      end Register_Log_Handler;

      procedure Proxy_Log_Handler
        (Device : Types.Evdev;
         Priority : Log_Priority;
         Data : System.Address;
         File : chars_Ptr;
         Line : int;
         Func : chars_Ptr;
         Format : chars_Ptr;
         Args : Types.Var_Args)
      is
         Log_Data : aliased User_Log_Data with Address => Data;

         function Format_To_Str
           (Format : chars_ptr;
            Args : Types.Var_Args)
           return chars_ptr
         with Import, Convention => C, Link_Name => "format_to_str";

         Ada_File : constant String := Value (File);
         Ada_Line : constant Integer := Integer (Line);
         Ada_Func : constant String := Value (Func);

         Message_Ptr : chars_ptr := Format_To_Str (Format, Args);
         Ada_Message : constant String := Value (Message_Ptr);
      begin
         Log_Handler
           (Raw_Device => Device,
            Priority => Priority,
            Data => Log_Data,
            File => Ada_File,
            Line => Ada_Line,
            Caller => Ada_Func,
            Message => Ada_Message);

         Free (Message_Ptr);
      end Proxy_Log_Handler;

   end Logging;

   -- Check_Negative_Errno

   procedure Check_Negative_Errno
     (Return_Value : int;
      Message_Prefix : String := "";
      Throw_Generic_Exception : Boolean := False;
      Evdev_Error_First : Boolean := False;
      Evdev_Message : String := "")
   is
   begin
      if (Return_Value >= 0) then
         return;
      end if;

      if Evdev_Error_First and (Return_Value = -1) then
         raise Evdev_Error with Evdev_Message;
      end if;

      Linux.Errno.Check_Errno_Value(-Return_Value, Message_Prefix, Throw_Generic_Exception);
   end;

   function Check_Boolean
     (Return_Value : int;
      Message : String)
     return Boolean is
   begin
      case Return_Value is
         when 0 => return True;
         when 1 => return False;
         when others => raise Evdev_Error with Message;
      end case;
   end;

   function Null_Value (Ptr : chars_ptr) return String is
   begin
      if Ptr = Null_Ptr then
         return "";
      else 
         return Value (Ptr);
      end if;
   end;

end Libevdev.Nice;
