generic
   type Underlying is mod <>;

package Generic_Flags is
   type Flags_Type is private;

   function To_Underlying (From : Flags_Type) return Underlying;

   function Add (To : Flags_Type; To_Add : Flags_Type) return Flags_Type;
   function "+" (Left : Flags_Type; Right : Flags_Type) return Flags_Type
      renames Add;

   function Remove (From : Flags_Type; To_Remove : Flags_Type) return Flags_Type;
   function "-" (Left : Flags_Type; Right : Flags_Type) return Flags_Type
      renames Remove;

   function Toggle (Inside : Flags_Type; To_Toggle : Flags_Type) return Flags_Type;
   function "*" (Left : Flags_Type; Right : Flags_Type) return Flags_Type
      renames Toggle;

   function Contains (Set : Flags_Type; Subset : Flags_Type) return Boolean;
   function ">" (Left : Flags_Type; Right : Flags_Type) return Boolean
      renames Contains;
   
   function Equal (Left : Flags_Type; Right : Flags_Type) return Boolean;
   function "=" (Left : Flags_Type; Right : Flags_Type) return Boolean
      renames Equal;

   generic
   package Unsafe
   is
      function From_Underlying (Base : Underlying) return Flags_Type;
   end;

   private
      type Flags_Type is new Underlying;

end Generic_Flags;
