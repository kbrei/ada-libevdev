with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings; use Interfaces.C.Strings;

package body Linux.Errno
is
   procedure Throw_Specific_Error (Value : Errno; Message : String)
   is
   begin
      -- it's a huge if, because case statements only work with static values
      if Value = E2BIG then
         raise Argument_List_Too_Long with Message;

      elsif Value = EACCES then
         raise Permission_Denied with Message;

      elsif Value = EADDRINUSE then
         raise Address_Already_In_Use with Message;

      elsif Value = EADDRNOTAVAIL then
         raise Address_Not_Available with Message;

      elsif Value = EAFNOSUPPORT then
         raise Address_Family_Not_Supported with Message;

      elsif Value = EAGAIN then
         raise Resource_Temporarily_Unavailable with Message;

      elsif Value = EALREADY then
         raise Connection_Already_In_Progress with Message;

      elsif Value = EBADE then
         raise Invalid_Exchange with Message;

      elsif Value = EBADF then
         raise Bad_File_Descriptor with Message;

      elsif Value = EBADFD then
         raise File_Descriptor_In_Bad_State with Message;

      elsif Value = EBADMSG then
         raise Bad_Message with Message;

      elsif Value = EBADR then
         raise Invalid_Request_Descriptor with Message;

      elsif Value = EBADRQC then
         raise Invalid_Request_Code with Message;

      elsif Value = EBADSLT then
         raise Invalid_Slot with Message;

      elsif Value = EBUSY then
         raise Device_Or_Resource_Busy with Message;

      elsif Value = ECANCELED then
         raise Operation_Canceled with Message;

      elsif Value = ECHILD then
         raise No_Child_Processes with Message;

      elsif Value = ECHRNG then
         raise Channel_Number_Out_Of_Range with Message;

      elsif Value = ECOMM then
         raise Communication_Error_On_Send with Message;

      elsif Value = ECONNABORTED then
         raise Connection_Aborted with Message;

      elsif Value = ECONNREFUSED then
         raise Connection_Refused with Message;

      elsif Value = ECONNRESET then
         raise Connection_Reset with Message;

      elsif Value = EDEADLK then
         raise Resource_Deadlock_Avoided with Message;

      elsif Value = EDEADLOCK then
         raise Resource_Deadlock_Avoided with Message;

      elsif Value = EDESTADDRREQ then
         raise Destination_Address_Required with Message;

      elsif Value = EDOM then
         raise Argument_Out_Of_Domain with Message;

      elsif Value = EDQUOT then
         raise Disk_Quota_Exceeded with Message;

      elsif Value = EEXIST then
         raise File_Exists with Message;

      elsif Value = EFAULT then
         raise Bad_Address with Message;

      elsif Value = EFBIG then
         raise File_Too_Large with Message;

      elsif Value = EHOSTDOWN then
         raise Host_Is_Down with Message;

      elsif Value = EHOSTUNREACH then
         raise Host_Is_Unreachable with Message;

      elsif Value = EIDRM then
         raise Identifier_Removed with Message;

      elsif Value = EILSEQ then
         raise Illegal_Byte_Sequence with Message;

      elsif Value = EINPROGRESS then
         raise Operation_In_Progress with Message;

      elsif Value = EINTR then
         raise Interrupted_Function_Call with Message;

      elsif Value = EINVAL then
         raise Invalid_Argument with Message;

      elsif Value = EIO then
         raise Input_Output_Error with Message;

      elsif Value = EISCONN then
         raise Socket_Is_Connected with Message;

      elsif Value = EISDIR then
         raise Is_A_Directory with Message;

      elsif Value = EISNAM then
         raise Is_A_Named_Type_File with Message;

      elsif Value = EKEYEXPIRED then
         raise Key_Has_Expired with Message;

      elsif Value = EKEYREJECTED then
         raise Key_Was_Rejected_By_Service with Message;

      elsif Value = EKEYREVOKED then
         raise Key_Has_Been_Revoked with Message;

      elsif Value = ELIBACC then
         raise Cannot_Access_Shared_Library with Message;

      elsif Value = ELIBBAD then
         raise Accessing_Corrupted_Library with Message;

      elsif Value = ELIBMAX then
         raise Too_Many_Shared_Libraries with Message;

      elsif Value = ELIBSCN then
         raise Lib_Section_Corrupted with Message;

      elsif Value = ELIBEXEC then
         raise Cannot_Exec_Library with Message;

      elsif Value = ELOOP then
         raise Too_Many_Symbolic_Links with Message;

      elsif Value = EMEDIUMTYPE then
         raise Wrong_Medium_Type with Message;

      elsif Value = EMFILE then
         raise Too_Many_Open_Files with Message;

      elsif Value = EMLINK then
         raise Too_Many_Links with Message;

      elsif Value = EMSGSIZE then
         raise Message_Too_Long with Message;

      elsif Value = EMULTIHOP then
         raise Multihop_Attempted with Message;

      elsif Value = ENAMETOOLONG then
         raise Filename_Too_Long with Message;

      elsif Value = ENETDOWN then
         raise Network_Is_Down with Message;

      elsif Value = ENETRESET then
         raise Connection_Aborted_By_Network with Message;

      elsif Value = ENETUNREACH then
         raise Network_Unreachable with Message;

      elsif Value = ENFILE then
         raise Too_Many_Open_Files_In_System with Message;

      elsif Value = ENOBUFS then
         raise No_Buffer_Space_Available with Message;

      elsif Value = ENODATA then
         raise No_Message_In_Queue with Message;

      elsif Value = ENODEV then
         raise No_Such_Device with Message;

      elsif Value = ENOENT then
         raise No_Such_File_Or_Directory with Message;

      elsif Value = ENOEXEC then
         raise Exec_Format_Error with Message;

      elsif Value = ENOKEY then
         raise Key_Not_Available with Message;

      elsif Value = ENOLCK then
         raise No_Locks_Available with Message;

      elsif Value = ENOLINK then
         raise Link_Has_Been_Severed with Message;

      elsif Value = ENOMEDIUM then
         raise No_Medium_Found with Message;

      elsif Value = ENOMEM then
         raise Not_Enough_Space with Message;

      elsif Value = ENOMSG then
         raise No_Message_Of_Correct_Type with Message;

      elsif Value = ENONET then
         raise No_Network with Message;

      elsif Value = ENOPKG then
         raise Package_Not_Installed with Message;

      elsif Value = ENOPROTOOPT then
         raise Protocol_Not_Available with Message;

      elsif Value = ENOSPC then
         raise No_Space_Left_On_Device with Message;

      elsif Value = ENOSR then
         raise No_STREAM_Resources with Message;

      elsif Value = ENOSTR then
         raise Not_A_STREAM with Message;

      elsif Value = ENOSYS then
         raise Function_Not_Implemented with Message;

      elsif Value = ENOTBLK then
         raise Block_Device_Required with Message;

      elsif Value = ENOTCONN then
         raise The_Socket_Is_Not_Connected with Message;

      elsif Value = ENOTDIR then
         raise Not_A_Directory with Message;

      elsif Value = ENOTEMPTY then
         raise Directory_Not_Empty with Message;

      elsif Value = ENOTSOCK then
         raise Not_A_Socket with Message;

      elsif Value = ENOTSUP then
         raise Operation_Not_Supported with Message;

      elsif Value = ENOTTY then
         raise Inappropriate_IO_Control_Operation with Message;

      elsif Value = ENOTUNIQ then
         raise Name_Not_Unique_On_Network with Message;

      elsif Value = ENXIO then
         raise No_Such_Device_Or_Address with Message;

      elsif Value = EOPNOTSUPP then
         raise Operation_Not_Supported_On_Socket with Message;

      elsif Value = EOVERFLOW then
         raise Overflow with Message;

      elsif Value = EPERM then
         raise Operation_Not_Permitted with Message;

      elsif Value = EPFNOSUPPORT then
         raise Protocol_Family_Not_Supported with Message;

      elsif Value = EPIPE then
         raise Broken_Pipe with Message;

      elsif Value = EPROTO then
         raise Protocol_Error with Message;

      elsif Value = EPROTONOSUPPORT then
         raise Protocol_Not_Supported with Message;

      elsif Value = EPROTOTYPE then
         raise Protocol_Wrong_Type_For_Socket with Message;

      elsif Value = ERANGE then
         raise Result_Too_Large with Message;

      elsif Value = EREMCHG then
         raise Remote_Address_Changed with Message;

      elsif Value = EREMOTE then
         raise Object_Is_Remote with Message;

      elsif Value = EREMOTEIO then
         raise Remote_IO_Error with Message;

      elsif Value = ERESTART then
         raise Interrupted_System_Needs_Restart with Message;

      elsif Value = EROFS then
         raise Read_only_Filesystem with Message;

      elsif Value = ESHUTDOWN then
         raise Transport_Endpoint_Shutdown with Message;

      elsif Value = ESPIPE then
         raise Invalid_Seek with Message;

      elsif Value = ESOCKTNOSUPPORT then
         raise Socket_Type_Not_Supported with Message;

      elsif Value = ESRCH then
         raise No_Such_Process with Message;

      elsif Value = ESTALE then
         raise Stale_File_Handle with Message;

      elsif Value = ESTRPIPE then
         raise Streams_Pipe_Error with Message;

      elsif Value = ETIME then
         raise Timer_Expired with Message;

      elsif Value = ETIMEDOUT then
         raise Connection_Timed_Out with Message;

      elsif Value = ETXTBSY then
         raise Text_File_Busy with Message;

      elsif Value = EUCLEAN then
         raise Structure_Needs_Cleaning with Message;

      elsif Value = EUNATCH then
         raise Protocol_Driver_Not_Attached with Message;

      elsif Value = EUSERS then
         raise Too_Many_Users with Message;

      elsif Value = EWOULDBLOCK then
         raise Operation_Would_Block with Message;

      elsif Value = EXDEV then
         raise Improper_Link with Message;

      elsif Value = EXFULL then
         raise Exchange_Full with Message;

      -- Default to unkown error
      else
         raise Some_Error with Message;
      end if;
   end;

   procedure Check_Errno
     (Return_Value : int;
      Message_Prefix : String := "";
      Throw_Generic_Exception : Boolean := False)
   is
   begin
      -- No error
      if Return_Value /= -1 then
         return;
      end if;

      Check_Errno_Value (int (Get_Errno), Message_Prefix, Throw_Generic_Exception);
   end;

   procedure Check_Errno_Value
     (Errno_Value : int;
      Message_Prefix : String := "";
      Throw_Generic_Exception : Boolean := False)
   is
   begin
      declare
         Current_Errno : constant Errno := Errno (Errno_Value);
         -- This should not be freed!
         Message_ptr : constant chars_ptr := Get_Errno_Message (Current_Errno);
         Message : constant String := Message_Prefix & Value (Message_ptr);
      begin
         if Throw_Generic_Exception then
            raise Some_Error with Message;
         else
            Throw_Specific_Error (Current_Errno, Message);
         end if;
      end;
   end;
end Linux.Errno;
