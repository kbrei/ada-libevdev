with Interfaces.C; use Interfaces.C;
with Interfaces.C.Strings; use Interfaces.C.Strings;

package Linux.Errno
is
   procedure Check_Errno
     (Return_Value : int;
      Message_Prefix : String := "";
      Throw_Generic_Exception : Boolean := False);

   procedure Check_Errno_Value
     (Errno_Value : int;
      Message_Prefix : String := "";
      Throw_Generic_Exception : Boolean := False);

   -- Exceptions which may occurr

   -- Used for unkown error codes and
   -- when Throw_Generic_Exception = True
   Some_Error : exception;

   -- "generated" out of man page
   Argument_List_Too_Long : exception; -- E2BIG

   Permission_Denied : exception; -- EACCES

   Address_Already_In_Use : exception; -- EADDRINUSE

   Address_Not_Available : exception; -- EADDRNOTAVAIL

   Address_Family_Not_Supported : exception; -- EAFNOSUPPORT

   Resource_Temporarily_Unavailable : exception; -- EAGAIN

   Connection_Already_In_Progress : exception; -- EALREADY

   Invalid_Exchange : exception; -- EBADE

   Bad_File_Descriptor : exception; -- EBADF

   File_Descriptor_In_Bad_State : exception; -- EBADFD

   Bad_Message : exception; -- EBADMSG

   Invalid_Request_Descriptor : exception; -- EBADR

   Invalid_Request_Code : exception; -- EBADRQC

   Invalid_Slot : exception; -- EBADSLT

   Device_Or_Resource_Busy : exception; -- EBUSY

   Operation_Canceled : exception; -- ECANCELED

   No_Child_Processes : exception; -- ECHILD

   Channel_Number_Out_Of_Range : exception; -- ECHRNG

   Communication_Error_On_Send : exception; -- ECOMM

   Connection_Aborted : exception; -- ECONNABORTED

   Connection_Refused : exception; -- ECONNREFUSED

   Connection_Reset : exception; -- ECONNRESET

   Resource_Deadlock_Avoided : exception; -- EDEADLK

   Destination_Address_Required : exception; -- EDESTADDRREQ

   Argument_Out_Of_Domain : exception; -- EDOM

   Disk_Quota_Exceeded : exception; -- EDQUOT

   File_Exists : exception; -- EEXIST

   Bad_Address : exception; -- EFAULT

   File_Too_Large : exception; -- EFBIG

   Host_Is_Down : exception; -- EHOSTDOWN

   Host_Is_Unreachable : exception; -- EHOSTUNREACH

   Identifier_Removed : exception; -- EIDRM

   Illegal_Byte_Sequence : exception; -- EILSEQ

   Operation_In_Progress : exception; -- EINPROGRESS

   Interrupted_Function_Call : exception; -- EINTR

   Invalid_Argument : exception; -- EINVAL

   Input_Output_Error : exception; -- EIO

   Socket_Is_Connected : exception; -- EISCONN

   Is_A_Directory : exception; -- EISDIR

   Is_A_Named_Type_File : exception; -- EISNAM

   Key_Has_Expired : exception; -- EKEYEXPIRED

   Key_Was_Rejected_By_Service : exception; -- EKEYREJECTED

   Key_Has_Been_Revoked : exception; -- EKEYREVOKED

   Cannot_Access_Shared_Library : exception; -- ELIBACC

   Accessing_Corrupted_Library : exception; -- ELIBBAD

   Too_Many_Shared_Libraries : exception; -- ELIBMAX

   Lib_Section_Corrupted : exception; -- ELIBSCN

   Cannot_Exec_Library : exception; -- ELIBEXEC

   Too_Many_Symbolic_Links : exception; -- ELOOP

   Wrong_Medium_Type : exception; -- EMEDIUMTYPE

   Too_Many_Open_Files : exception; -- EMFILE

   Too_Many_Links : exception; -- EMLINK

   Message_Too_Long : exception; -- EMSGSIZE

   Multihop_Attempted : exception; -- EMULTIHOP

   Filename_Too_Long : exception; -- ENAMETOOLONG

   Network_Is_Down : exception; -- ENETDOWN

   Connection_Aborted_By_Network : exception; -- ENETRESET

   Network_Unreachable : exception; -- ENETUNREACH

   Too_Many_Open_Files_In_System : exception; -- ENFILE

   No_Buffer_Space_Available : exception; -- ENOBUFS

   No_Message_In_Queue : exception; -- ENODATA

   No_Such_Device : exception; -- ENODEV

   No_Such_File_Or_Directory : exception; -- ENOENT

   Exec_Format_Error : exception; -- ENOEXEC

   Key_Not_Available : exception; -- ENOKEY

   No_Locks_Available : exception; -- ENOLCK

   Link_Has_Been_Severed : exception; -- ENOLINK

   No_Medium_Found : exception; -- ENOMEDIUM

   Not_Enough_Space : exception; -- ENOMEM

   No_Message_Of_Correct_Type : exception; -- ENOMSG

   No_Network : exception; -- ENONET

   Package_Not_Installed : exception; -- ENOPKG

   Protocol_Not_Available : exception; -- ENOPROTOOPT

   No_Space_Left_On_Device : exception; -- ENOSPC

   No_STREAM_Resources : exception; -- ENOSR

   Not_A_STREAM : exception; -- ENOSTR

   Function_Not_Implemented : exception; -- ENOSYS

   Block_Device_Required : exception; -- ENOTBLK

   The_Socket_Is_Not_Connected : exception; -- ENOTCONN

   Not_A_Directory : exception; -- ENOTDIR

   Directory_Not_Empty : exception; -- ENOTEMPTY

   Not_A_Socket : exception; -- ENOTSOCK

   Operation_Not_Supported : exception; -- ENOTSUP

   Inappropriate_IO_Control_Operation : exception; -- ENOTTY

   Name_Not_Unique_On_Network : exception; -- ENOTUNIQ

   No_Such_Device_Or_Address : exception; -- ENXIO

   Operation_Not_Supported_On_Socket : exception; -- EOPNOTSUPP

   Overflow : exception; -- EOVERFLOW

   Operation_Not_Permitted : exception; -- EPERM

   Protocol_Family_Not_Supported : exception; -- EPFNOSUPPORT

   Broken_Pipe : exception; -- EPIPE

   Protocol_Error : exception; -- EPROTO

   Protocol_Not_Supported : exception; -- EPROTONOSUPPORT

   Protocol_Wrong_Type_For_Socket : exception; -- EPROTOTYPE

   Result_Too_Large : exception; -- ERANGE

   Remote_Address_Changed : exception; -- EREMCHG

   Object_Is_Remote : exception; -- EREMOTE

   Remote_IO_Error : exception; -- EREMOTEIO

   Interrupted_System_Needs_Restart : exception; -- ERESTART

   Read_only_Filesystem : exception; -- EROFS

   Transport_Endpoint_Shutdown : exception; -- ESHUTDOWN

   Invalid_Seek : exception; -- ESPIPE

   Socket_Type_Not_Supported : exception; -- ESOCKTNOSUPPORT

   No_Such_Process : exception; -- ESRCH

   Stale_File_Handle : exception; -- ESTALE

   Streams_Pipe_Error : exception; -- ESTRPIPE

   Timer_Expired : exception; -- ETIME

   Connection_Timed_Out : exception; -- ETIMEDOUT

   Text_File_Busy : exception; -- ETXTBSY

   Structure_Needs_Cleaning : exception; -- EUCLEAN

   Protocol_Driver_Not_Attached : exception; -- EUNATCH

   Too_Many_Users : exception; -- EUSERS

   Operation_Would_Block : exception; -- EWOULDBLOCK

   Improper_Link : exception; -- EXDEV

   Exchange_Full : exception; -- EXFULL

   type Errno is new int;

   function Get_Errno_Message (Value : Errno) return chars_ptr with
     Import, Convention => C, Link_Name => "strerror";

   function Get_Errno return Errno with
     Import, Convention => C, Link_Name => "get_errno";

   E2BIG : constant Errno with
     Import, Convention => C, Link_Name => "e2big";

   EACCES : constant Errno with
     Import, Convention => C, Link_Name => "eacces";

   EADDRINUSE : constant Errno with
     Import, Convention => C, Link_Name => "eaddrinuse";

   EADDRNOTAVAIL : constant Errno with
     Import, Convention => C, Link_Name => "eaddrnotavail";

   EAFNOSUPPORT : constant Errno with
     Import, Convention => C, Link_Name => "eafnosupport";

   EAGAIN : constant Errno with
     Import, Convention => C, Link_Name => "eagain";

   EALREADY : constant Errno with
     Import, Convention => C, Link_Name => "ealready";

   EBADE : constant Errno with
     Import, Convention => C, Link_Name => "ebade";

   EBADF : constant Errno with
     Import, Convention => C, Link_Name => "ebadf";

   EBADFD : constant Errno with
     Import, Convention => C, Link_Name => "ebadfd";

   EBADMSG : constant Errno with
     Import, Convention => C, Link_Name => "ebadmsg";

   EBADR : constant Errno with
     Import, Convention => C, Link_Name => "ebadr";

   EBADRQC : constant Errno with
     Import, Convention => C, Link_Name => "ebadrqc";

   EBADSLT : constant Errno with
     Import, Convention => C, Link_Name => "ebadslt";

   EBUSY : constant Errno with
     Import, Convention => C, Link_Name => "ebusy";

   ECANCELED : constant Errno with
     Import, Convention => C, Link_Name => "ecanceled";

   ECHILD : constant Errno with
     Import, Convention => C, Link_Name => "echild";

   ECHRNG : constant Errno with
     Import, Convention => C, Link_Name => "echrng";

   ECOMM : constant Errno with
     Import, Convention => C, Link_Name => "ecomm";

   ECONNABORTED : constant Errno with
     Import, Convention => C, Link_Name => "econnaborted";

   ECONNREFUSED : constant Errno with
     Import, Convention => C, Link_Name => "econnrefused";

   ECONNRESET : constant Errno with
     Import, Convention => C, Link_Name => "econnreset";

   EDEADLK : constant Errno with
     Import, Convention => C, Link_Name => "edeadlk";

   EDEADLOCK : constant Errno with
     Import, Convention => C, Link_Name => "edeadlock";

   EDESTADDRREQ : constant Errno with
     Import, Convention => C, Link_Name => "edestaddrreq";

   EDOM : constant Errno with
     Import, Convention => C, Link_Name => "edom";

   EDQUOT : constant Errno with
     Import, Convention => C, Link_Name => "edquot";

   EEXIST : constant Errno with
     Import, Convention => C, Link_Name => "eexist";

   EFAULT : constant Errno with
     Import, Convention => C, Link_Name => "efault";

   EFBIG : constant Errno with
     Import, Convention => C, Link_Name => "efbig";

   EHOSTDOWN : constant Errno with
     Import, Convention => C, Link_Name => "ehostdown";

   EHOSTUNREACH : constant Errno with
     Import, Convention => C, Link_Name => "ehostunreach";

   EIDRM : constant Errno with
     Import, Convention => C, Link_Name => "eidrm";

   EILSEQ : constant Errno with
     Import, Convention => C, Link_Name => "eilseq";

   EINPROGRESS : constant Errno with
     Import, Convention => C, Link_Name => "einprogress";

   EINTR : constant Errno with
     Import, Convention => C, Link_Name => "eintr";

   EINVAL : constant Errno with
     Import, Convention => C, Link_Name => "einval";

   EIO : constant Errno with
     Import, Convention => C, Link_Name => "eio";

   EISCONN : constant Errno with
     Import, Convention => C, Link_Name => "eisconn";

   EISDIR : constant Errno with
     Import, Convention => C, Link_Name => "eisdir";

   EISNAM : constant Errno with
     Import, Convention => C, Link_Name => "eisnam";

   EKEYEXPIRED : constant Errno with
     Import, Convention => C, Link_Name => "ekeyexpired";

   EKEYREJECTED : constant Errno with
     Import, Convention => C, Link_Name => "ekeyrejected";

   EKEYREVOKED : constant Errno with
     Import, Convention => C, Link_Name => "ekeyrevoked";

   EL2HLT : constant Errno with
     Import, Convention => C, Link_Name => "el2hlt";

   EL2NSYNC : constant Errno with
     Import, Convention => C, Link_Name => "el2nsync";

   EL3HLT : constant Errno with
     Import, Convention => C, Link_Name => "el3hlt";

   EL3RST : constant Errno with
     Import, Convention => C, Link_Name => "el3rst";

   ELIBACC : constant Errno with
     Import, Convention => C, Link_Name => "elibacc";

   ELIBBAD : constant Errno with
     Import, Convention => C, Link_Name => "elibbad";

   ELIBMAX : constant Errno with
     Import, Convention => C, Link_Name => "elibmax";

   ELIBSCN : constant Errno with
     Import, Convention => C, Link_Name => "elibscn";

   ELIBEXEC : constant Errno with
     Import, Convention => C, Link_Name => "elibexec";

   ELOOP : constant Errno with
     Import, Convention => C, Link_Name => "eloop";

   EMEDIUMTYPE : constant Errno with
     Import, Convention => C, Link_Name => "emediumtype";

   EMFILE : constant Errno with
     Import, Convention => C, Link_Name => "emfile";

   EMLINK : constant Errno with
     Import, Convention => C, Link_Name => "emlink";

   EMSGSIZE : constant Errno with
     Import, Convention => C, Link_Name => "emsgsize";

   EMULTIHOP : constant Errno with
     Import, Convention => C, Link_Name => "emultihop";

   ENAMETOOLONG : constant Errno with
     Import, Convention => C, Link_Name => "enametoolong";

   ENETDOWN : constant Errno with
     Import, Convention => C, Link_Name => "enetdown";

   ENETRESET : constant Errno with
     Import, Convention => C, Link_Name => "enetreset";

   ENETUNREACH : constant Errno with
     Import, Convention => C, Link_Name => "enetunreach";

   ENFILE : constant Errno with
     Import, Convention => C, Link_Name => "enfile";

   ENOBUFS : constant Errno with
     Import, Convention => C, Link_Name => "enobufs";

   ENODATA : constant Errno with
     Import, Convention => C, Link_Name => "enodata";

   ENODEV : constant Errno with
     Import, Convention => C, Link_Name => "enodev";

   ENOENT : constant Errno with
     Import, Convention => C, Link_Name => "enoent";

   ENOEXEC : constant Errno with
     Import, Convention => C, Link_Name => "enoexec";

   ENOKEY : constant Errno with
     Import, Convention => C, Link_Name => "enokey";

   ENOLCK : constant Errno with
     Import, Convention => C, Link_Name => "enolck";

   ENOLINK : constant Errno with
     Import, Convention => C, Link_Name => "enolink";

   ENOMEDIUM : constant Errno with
     Import, Convention => C, Link_Name => "enomedium";

   ENOMEM : constant Errno with
     Import, Convention => C, Link_Name => "enomem";

   ENOMSG : constant Errno with
     Import, Convention => C, Link_Name => "enomsg";

   ENONET : constant Errno with
     Import, Convention => C, Link_Name => "enonet";

   ENOPKG : constant Errno with
     Import, Convention => C, Link_Name => "enopkg";

   ENOPROTOOPT : constant Errno with
     Import, Convention => C, Link_Name => "enoprotoopt";

   ENOSPC : constant Errno with
     Import, Convention => C, Link_Name => "enospc";

   ENOSR : constant Errno with
     Import, Convention => C, Link_Name => "enosr";

   ENOSTR : constant Errno with
     Import, Convention => C, Link_Name => "enostr";

   ENOSYS : constant Errno with
     Import, Convention => C, Link_Name => "enosys";

   ENOTBLK : constant Errno with
     Import, Convention => C, Link_Name => "enotblk";

   ENOTCONN : constant Errno with
     Import, Convention => C, Link_Name => "enotconn";

   Enotdir : constant Errno with
     Import, Convention => C, Link_Name => "enotdir";

   ENOTEMPTY : constant Errno with
     Import, Convention => C, Link_Name => "enotempty";

   ENOTSOCK : constant Errno with
     Import, Convention => C, Link_Name => "enotsock";

   ENOTSUP : constant Errno with
     Import, Convention => C, Link_Name => "enotsup";

   ENOTTY : constant Errno with
     Import, Convention => C, Link_Name => "enotty";

   ENOTUNIQ : constant Errno with
     Import, Convention => C, Link_Name => "enotuniq";

   ENXIO : constant Errno with
     Import, Convention => C, Link_Name => "enxio";

   EOPNOTSUPP : constant Errno with
     Import, Convention => C, Link_Name => "eopnotsupp";

   EOVERFLOW : constant Errno with
     Import, Convention => C, Link_Name => "eoverflow";

   EPERM : constant Errno with
     Import, Convention => C, Link_Name => "eperm";

   EPFNOSUPPORT : constant Errno with
     Import, Convention => C, Link_Name => "epfnosupport";

   EPIPE : constant Errno with
     Import, Convention => C, Link_Name => "epipe";

   EPROTO : constant Errno with
     Import, Convention => C, Link_Name => "eproto";

   EPROTONOSUPPORT : constant Errno with
     Import, Convention => C, Link_Name => "eprotonosupport";

   EPROTOTYPE : constant Errno with
     Import, Convention => C, Link_Name => "eprototype";

   ERANGE : constant Errno with
     Import, Convention => C, Link_Name => "erange";

   EREMCHG : constant Errno with
     Import, Convention => C, Link_Name => "eremchg";

   EREMOTE : constant Errno with
     Import, Convention => C, Link_Name => "eremote";

   EREMOTEIO : constant Errno with
     Import, Convention => C, Link_Name => "eremoteio";

   ERESTART : constant Errno with
     Import, Convention => C, Link_Name => "erestart";

   EROFS : constant Errno with
     Import, Convention => C, Link_Name => "erofs";

   ESHUTDOWN : constant Errno with
     Import, Convention => C, Link_Name => "eshutdown";

   ESPIPE : constant Errno with
     Import, Convention => C, Link_Name => "espipe";

   ESOCKTNOSUPPORT : constant Errno with
     Import, Convention => C, Link_Name => "esocktnosupport";

   ESRCH : constant Errno with
     Import, Convention => C, Link_Name => "esrch";

   ESTALE : constant Errno with
     Import, Convention => C, Link_Name => "estale";

   ESTRPIPE : constant Errno with
     Import, Convention => C, Link_Name => "estrpipe";

   ETIME : constant Errno with
     Import, Convention => C, Link_Name => "etime";

   ETIMEDOUT : constant Errno with
     Import, Convention => C, Link_Name => "etimedout";

   ETXTBSY : constant Errno with
     Import, Convention => C, Link_Name => "etxtbsy";

   EUCLEAN : constant Errno with
     Import, Convention => C, Link_Name => "euclean";

   EUNATCH : constant Errno with
     Import, Convention => C, Link_Name => "eunatch";

   EUSERS : constant Errno with
     Import, Convention => C, Link_Name => "eusers";

   EWOULDBLOCK : constant Errno with
     Import, Convention => C, Link_Name => "ewouldblock";

   EXDEV : constant Errno with
     Import, Convention => C, Link_Name => "exdev";

   EXFULL : constant Errno with
     Import, Convention => C, Link_Name => "exfull";

end Linux.Errno;
