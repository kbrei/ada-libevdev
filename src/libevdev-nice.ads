with Interfaces.C.Strings; use Interfaces.C.Strings; 
with Interfaces.C; use Interfaces.C;
with System;

with Ada.Finalization;

with Libevdev.Types; use Libevdev.Types;
with Linux.File;
with Linux.Errno;

package Libevdev.Nice is

   Evdev_Error : exception;
   
   type Evdev is new Ada.Finalization.Controlled with private;

   function New_Evdev
     (File_Descriptor : Linux.File.Descriptor)
     return Evdev;

   function New_Evdev
     (File_Path : String;
      Blocking : Boolean := False)
     return Evdev;

   function File_Descriptor (Device : Evdev) return Linux.File.Descriptor;

   procedure Run_Raw
     (Device : Evdev;
      Action : not null access procedure
        (Raw_Device : Libevdev.Types.Evdev));

   procedure Grab (Device : Evdev; Mode : Grab_Mode);

   -- Precoditions that check the FD (O_NONBLOCK)!
   function Next_Event
     (Device : Evdev;
      Flags : Read_Flags;
      Event : out Libevdev.Types.Event)
     return Read_Status;

   function Has_Event_Pending (Device : Evdev) return Boolean;
   
   function Name (Device : Evdev) return String;
   function Physical_Location (Device : Evdev) return String;
   function Unique (Device : Evdev) return String;
   function Product_ID (Device : Evdev) return Integer;
   function Vendor_ID (Device : Evdev) return Integer;
   function Bustype_ID (Device : Evdev) return Integer;
   function Version_ID (Device : Evdev) return Integer;
   function Driver_Version (Device : Evdev) return Integer;

   function Has_Property (Device : Evdev; Prop : Property) return Boolean;
   
   function Has_Event_Type (Device : Evdev; Event_Type : Types.Event_Type) return Boolean;
   function Has_Event (Device : Evdev; Kind : Event_Kind) return Boolean;

   function Absolute_Info (Device : Evdev; Code : Absolute_Axis_Code) return Absolute_Axis_Info;

   procedure Set_Absolute_Info
     (Device : Evdev;
      Code : Absolute_Axis_Code;
      Value : Absolute_Axis_Info);

   procedure Change_Absolute_Info
     (Device : Evdev;
      Code : Absolute_Axis_Code;
      Change : not null access procedure
        (Info : in out Absolute_Axis_Info));

   function Value
     (Device : Evdev;
      Event : Event_Kind)
     return Integer
   with
     Pre => Device.Has_Event (Event) and (not Is_Multi_Touch (Event));
   
   procedure Set_Value
     (Device : Evdev;
      Event : Event_Kind;
      Value : Integer)
   with
     Pre => Device.Has_Event (Event) and Event.Event_Type /= Relative_Axis;
   
   function Slot_Value
     (Device : Evdev;
      Slot : Natural;
      Code : Multi_Touch_Code)
     return Integer
   with 
     Pre => Device.Supports_Multi_Touch and Device.Num_Slots <= Slot;

   procedure Set_Slot_Value
     (Device : Evdev;
      Slot : Natural;
      Code : Multi_Touch_Code;
      Value : Integer);

   function Supports_Multi_Touch (Device : Evdev) return Boolean;

   function Num_Slots (Device : Evdev) return Natural with
     Pre => Device.Supports_Multi_Touch;

   function Current_Slot (Device : Evdev) return Natural with
     pre => Device.Supports_Multi_Touch;
   

   procedure Enable_Event_Type (Device : Evdev; Event_Type : Types.Event_Type);
   procedure Disable_Event_Type (Device : Evdev; Event_Type : Types.Event_Type);
   
   type Event_Config (Event_Type : Libevdev.Types.Event_Type) is record
      case Event_Type is
         when Absolute_Axis => Abs_Info : Absolute_Axis_Info;
         when Relative_Axis => Rel_Info : Integer;
         when others => null;
      end case;
   end record;

   procedure Enable_Event
     (Device : Evdev;
      Event : Event_Kind;
      Config : Event_Config)
   with 
     Pre => Event.Event_Type = Config.Event_Type;
   
   procedure Disable_Event_Code
     (Device : Evdev;
      Event : Event_Kind);

   procedure Set_Led_Value
     (Device : Evdev;
      Code : Led_Code;
      Value : Led_Value);

   type Led_Action is record
      Code : Led_Code;
      State : Led_Value;
   end record with
     Convention => C_Pass_By_Copy;

   type Led_Actions is array (Integer range <>) of Led_Action with
     Convention => C;

   procedure Set_Led_Values (Device : Evdev; Actions : Led_Actions);

   procedure Set_Clock_Id (Device : Evdev; Clock_Id : Clock_Type);

   generic
      type User_Log_Data is private;
      type User_Log_Data_Access is access User_Log_Data;
      with procedure Log_Handler
        (Raw_Device : Types.Evdev;
         Priority : Log_Priority;
         Data : in out User_Log_Data;
         File : String;
         Line : Integer;
         Caller : String;
         Message : String);
   package Logging is

      procedure Register_Log_Handler
        (Device : in out Evdev;
         Priority : Log_Priority := Error;
         Associated_Data : User_Log_Data_Access);

   private

      procedure Proxy_Log_Handler
        (Device : Types.Evdev;
         Priority : Log_Priority;
         Data : System.Address;
         File : chars_Ptr;
         Line : int;
         Func : chars_Ptr;
         Format : chars_Ptr;
         Args : Types.Var_Args)
      with Convention => C;

      -- WORKAROUND
      Proxy_Log_Handler_Access : constant Device_Log_Func :=
        Proxy_Log_Handler'Unrestricted_Access;

   end Logging;

private
   use Interfaces.C;
   use Interfaces.C.Strings;

   type Evdev is new Ada.Finalization.Controlled with record
      Raw_Device : Types.Evdev;
   end record;

   overriding procedure Finalize (Device : in out Evdev);

   procedure Check_Negative_Errno
     (Return_Value : int;
      Message_Prefix : String := "";
      Throw_Generic_Exception : Boolean := False;
      Evdev_Error_First : Boolean := False;
      Evdev_Message : String := "");

   function Check_Boolean
     (Return_Value : int;
      Message : String)
      return Boolean;

   function Null_Value (Ptr : chars_ptr) return String;
   
end Libevdev.Nice;
